package com.woate.talkingbird.dto;

public class OHJ190DTO {
	String bankNo;
	String prodId;
	public String getBankNo() {
		return bankNo;
	}
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	@Override
	public String toString() {
		return "OHJ190DTO [bankNo=" + bankNo + ", prodId=" + prodId + "]";
	}
	
}
