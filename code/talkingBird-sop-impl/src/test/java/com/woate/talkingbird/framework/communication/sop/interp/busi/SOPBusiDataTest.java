package com.woate.talkingbird.framework.communication.sop.interp.busi;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.dto.ERR000DTO;
import com.woate.talkingbird.dto.O900b0DTO;
import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.busi.SOPBusiData;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.EmptyMetadataLoaderMock1;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.MainMetadataLoaderMock1;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.O900b1;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPBeanImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPBeanWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class SOPBusiDataTest {
	@Test
	public void testFormat1() throws TransformException{
		SOPSession.destory();
		final List<SOPMetadata> list = new ArrayList<SOPMetadata>();
		SOPBeanWriteable obj0 = new SOPBeanImpl("O900b0","", "交换密钥","UTF-8", O900b1.class);
		SOPVariableFieldWriteable field0 = new SOPVariableFieldBuilder("name").setAlias("密钥名0").setMinLength(1).setMaxLength(12).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		
		obj0.addField(field0);
		list.add(obj0);
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public String getTradeCode() {
				return "900b";
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				return list;
			}

			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> headers = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("sopname").setAlias("SOP名称").setLength(10).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				headers.add(field1);
				return headers;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
			
		};
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		SOPBusiWrapper busiWrapper= session.createBusiData();
		O900b1 o900b1 = new O900b1();
		o900b1.name = "这是一个";
		busiWrapper.setBusidata("O900b0", o900b1);
		SOPBusiData soBusiData = new SOPBusiData("900b", loader);
		ByteBuffer buffer = soBusiData.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("00000000000000000000064F39303062300CE8BF99E698AFE4B880E4B8AA"), buffer.array()));
		SOPSession.destory();
	}
	@Test
	public void testFormat2() throws TransformException{
		SOPSession.destory();
		final List<SOPMetadata> list = new ArrayList<SOPMetadata>();
		SOPBeanWriteable obj0 = new SOPBeanImpl("O900b0","","交换密钥","UTF-8", O900b1.class);
		SOPVariableFieldWriteable field0 = new SOPVariableFieldBuilder("name").setAlias("密钥名0").setMinLength(1).setMaxLength(12).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		obj0.addField(field0);
		list.add(obj0);
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public String getTradeCode() {
				return "900b";
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				return list;
			}

			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> headers = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("sopname").setAlias("SOP名称").setLength(10).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				headers.add(field1);
				return headers;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}


			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
			
		};
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		SOPBusiWrapper busiWrapper= session.createBusiData();
		O900b1 o900b1 = new O900b1();
		o900b1.name = "这是一个";
		busiWrapper.setBusidata("O900b0", o900b1);
		busiWrapper.setAttribute("sopname", "1234561234");
		SOPBusiData soBusiData = new SOPBusiData("900b", loader);
		ByteBuffer buffer = soBusiData.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("31323334353631323334064F39303062300CE8BF99E698AFE4B880E4B8AA"), buffer.array()));
		SOPSession.destory();
	}
	
	@Test
	public void testParse1() throws TransformException{
		SOPSession.destory();
		final List<SOPMetadata> list = new ArrayList<SOPMetadata>();
		SOPBeanWriteable obj0 = new SOPBeanImpl("O900b0","", "交换密钥","UTF-8", O900b1.class);
		SOPVariableFieldWriteable field0 = new SOPVariableFieldBuilder("name").setAlias("密钥名0").setMinLength(1).setMaxLength(12).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		obj0.addField(field0);
		list.add(obj0);
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public String getTradeCode() {
				return "900b";
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				return list;
			}

			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> headers = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("sopname").setAlias("SOP名称").setLength(10).setClazz( String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				headers.add(field1);
				return headers;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
			
		};

		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		SOPBusiWrapper busiWrapper= session.createBusiData();
		O900b1 o900b1 = new O900b1();
		o900b1.name = "123456";
		busiWrapper.setBusidata("O900b0", o900b1);
		busiWrapper.setAttribute("sopname", "123456");
		SOPBusiData soBusiData = new SOPBusiData("900b", loader);
		ByteBuffer buffer = soBusiData.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("00000000313233343536064F393030623006313233343536"), buffer.array()));
		buffer.rewind();
		session.busiDatas().clear();
		Assert.assertTrue(session.busiDatas().isEmpty());
		soBusiData.parse(buffer);
		Assert.assertEquals("123456", session.peekFirstBusiData().getAttribute("sopname"));
		O900b1 o900b1_new = session.peekFirstBusiData().getBusidata("O900b0");
		Assert.assertEquals("123456", o900b1_new.name);
		SOPSession.destory();
	}
	
	@Test
	public void testFormat4() throws TransformException{
		SOPSession.destory();
		format1();
		SOPSession.destory();
	}
	private byte[] format1() throws TransformException {
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public String getTradeCode() {
				return "ERRO";
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				List<SOPMetadata> list = new ArrayList<SOPMetadata>();
				SOPBeanWriteable obj0 = new SOPBeanImpl("ERR000","","错误信息","UTF-8", ERR000DTO.class);
				SOPVariableFieldWriteable field0 = new SOPVariableFieldBuilder("errorNo").setAlias("错误号").setMinLength(0).setMaxLength(0).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("errorCode").setAlias("错误码").setMinLength(7).setMaxLength(7).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("errorMsg").setAlias("错误信息").setMinLength(1).setMaxLength(20).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				obj0.addField(field0);
				obj0.addField(field1);
				obj0.addField(field2);
				list.add(obj0);
				return list;
			}

			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				return new ArrayList<SOPMetadata>();
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}
			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}};
		SOPBusiData busiData = new SOPBusiData(loader.getTradeCode(), loader);
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		SOPBusiWrapper busiWrapper= session.createBusiData();
		ERR000DTO erro = new ERR000DTO();
		erro.setErrorCode("GT00001");
		erro.setErrorMsg("123321123");
		busiWrapper.setBusidata("ERR000", erro);
		
		ByteBuffer buffer = busiData.format();
		return buffer.array();
	}
	
	@Test
	public void testParse4() throws TransformException{
		SOPSession.destory();
		byte[] bytes =  format1();
		SOPSession.destory();
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public String getTradeCode() {
				return "ERRO";
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				List<SOPMetadata> list = new ArrayList<SOPMetadata>();
				SOPBeanWriteable obj0 = new SOPBeanImpl("ERR000","","错误信息","UTF-8", ERR000DTO.class);
				SOPVariableFieldWriteable field0 = new SOPVariableFieldBuilder("errorNo").setAlias("错误号").setMinLength(0).setMaxLength(0).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("errorCode").setAlias("错误码").setMinLength(7).setMaxLength(7).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("errorMsg").setAlias("错误信息").setMinLength(1).setMaxLength(20).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				obj0.addField(field0);
				obj0.addField(field1);
				obj0.addField(field2);
				list.add(obj0);
				return list;
			}

			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				return new ArrayList<SOPMetadata>();
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}


			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}};
		SOPBusiData busiData = new SOPBusiData(loader.getTradeCode(), loader);
		SOPSession.destory();
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.rewind();
		Assert.assertTrue(session.busiDatas().isEmpty());
		busiData.parse(buffer);
		Assert.assertTrue(!session.busiDatas().isEmpty());
		ERR000DTO erro = session.peekFirstBusiData().getBusidata("ERR000");
		Assert.assertEquals("GT00001", erro.getErrorCode());
		Assert.assertEquals("123321123", erro.getErrorMsg());
	}
}
