package com.woate.talkingbird.framework.communication.sop.interp.header.delegate;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.BusiHeaderFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.BusiHeaderParserDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class BusiHeaderParserRDelegateTest {
	@Test
	public void testParse1(){
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}
			
			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> list = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("name").setAlias("姓名").setLength(6).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				list.add(field1);
				return list;
			}

			@Override
			public String getTradeCode() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}


			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
		};
		
		BusiHeaderFormatterDelegate formatter = new BusiHeaderFormatterDelegate(loader);
		ByteBuffer buffer = null;
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		SOPBusiWrapper busiWrapper = session.createBusiData();
		busiWrapper.setAttribute("name", "123456");
		try {
			buffer = formatter.format();
		} catch (TransformException e) {
			e.printStackTrace();
		}
		session.pollFirstBusiData();
		Assert.assertTrue(session.busiDatas().isEmpty());
		session.createBusiData();
		Assert.assertTrue(!session.busiDatas().isEmpty());
		BusiHeaderParserDelegate parse = new BusiHeaderParserDelegate(loader);
		buffer.rewind();
		try {
			parse.parse(buffer);
		} catch (TransformException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("123456", session.peekFirstBusiData().getAttribute("name"));
		SOPSession.destory();
	}
	@Test
	public void testParse2(){
		BusiDataMetadateLoader loader = new BusiDataMetadateLoader(){
			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}
			
			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> list = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("name").setAlias("姓名").setLength(5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				list.add(field1);
				return list;
			}

			@Override
			public String getTradeCode() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<SOPMetadata> list(PacketType type) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}
			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
		};
		
		BusiHeaderFormatterDelegate formatter = new BusiHeaderFormatterDelegate(loader);
		ByteBuffer buffer = null;
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		SOPBusiWrapper busiWrapper = session.createBusiData();
		busiWrapper.setAttribute("name", "12345");
		try {
			buffer = formatter.format();
		} catch (TransformException e) {
			e.printStackTrace();
		}
		session.pollFirstBusiData();
		Assert.assertTrue(session.busiDatas().isEmpty());
		BusiHeaderParserDelegate parse = new BusiHeaderParserDelegate(loader);
		session.createBusiData();
		buffer.rewind();
		try {
			parse.parse(buffer);
		} catch (TransformException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("12345", session.peekFirstBusiData().getAttribute("name"));
		SOPSession.destory();
	}
}
