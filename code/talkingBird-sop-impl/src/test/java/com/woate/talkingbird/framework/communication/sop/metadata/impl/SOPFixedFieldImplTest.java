package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPFixedFieldWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2DateConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Date2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.DateUtil;
import com.woate.talkingbird.framework.util.Hex;

public class SOPFixedFieldImplTest {
	@Test
	public void testFormat1() throws Exception {
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setLength(5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		String m = "12345";
		// 20
		byte[] bytes = field.format(m);
		System.out.println(Hex.toDisplayString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("3132333435"), bytes));
	}

	@Test
	public void testFormat2() throws Exception {
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setLength(265).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		String m = "";
		String hex = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
			hex += "3132333435";
		}
		byte[] bytes = field.format(m);
		System.out.println(Hex.toDisplayString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}

	@Test
	public void testFormat3() throws Exception {
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setLength(14).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		String m = "这其实是个测试";
		byte[] bytes = field.format(m);
		System.out.println(Hex.toDisplayString(bytes));
		String hex = "E8BF99E585B6E5AE9EE698AFE4B8AAE6B58BE8AF95";
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}
	@Test
	public void testFormat4() throws Exception {
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setLength(1).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		String m = "这其实是个测试";
		byte[] bytes = field.format(m);
	}
	@Test
	public void testFormat5() throws Exception {
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setLength(15).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		String m = "这其实是个测试";
		byte[] bytes = field.format(m);
		System.out.println(Hex.toDisplayString(bytes));
		String hex = "E8BF99E585B6E5AE9EE698AFE4B8AAE6B58BE8AF95";
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}

	@Test
	public void testParse1() throws Exception {
		String msg = "12345";
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setLength(5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(msg);
		ByteBuffer input = ByteBuffer.wrap(bytes);
		Object val = field.parse(input);
		Assert.assertEquals(msg, val);
	}

	@Test
	public void testParse2() throws Exception {
		String msg = "这其实是个测试";
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setEncoding("GBK").setLength(msg.getBytes().length).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(msg);
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.rewind();
		Object val = field.parse(buffer);
		Assert.assertEquals(msg, val);
	}

	@Test
	public void testParse3() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setEncoding("GBK").setLength(53*5).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(m);
		Object val = field.parse(ByteBuffer.wrap(bytes));
		Assert.assertEquals(m, val);
	}
	@Test
	public void testParse4() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setEncoding("GBK").setLength(53*5+3).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(m);
		Object val = field.parse(ByteBuffer.wrap(bytes));
	}
	@Test
	public void testParse5() throws Exception {
		String m = "";
		for (int i = 0; i < 53; i++) {
			m += "12345";
		}
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER").setAlias("交易员").setEncoding("GBK").setLength(53*5-1).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		byte[] bytes = field.format(m);
		Object val = field.parse(ByteBuffer.wrap(bytes));
		Assert.assertEquals(m.substring(0, m.length() -1), val);
	}
	@Test
	public void testFormat6() throws TransformException{
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER_DATE").setAlias("交易日期").setLength(8).setClazz(Date.class).setFormatConverter(new Date2ByteArrayConverter()).setParseConverter(new ByteArray2DateConverter()).setValueMode(ValueMode.USER).build();
		String hex = "3230313530313031";
		byte[] bytes = field.format(DateUtil.toDate("20150101"));
		System.out.println(Hex.toHexString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString(hex), bytes));
	}
	@Test
	public void testParse6() throws TransformException{
		SOPFixedField field = new SOPFixedFieldBuilder("TRADER_DATE").setAlias("交易日期").setLength(8).setClazz(Date.class).setFormatConverter(new Date2ByteArrayConverter()).setParseConverter(new ByteArray2DateConverter()).setValueMode(ValueMode.USER).build();
		String hex = "3230313530313031";
		byte[] bytes = Hex.fromHexString(hex);
		Date obj = (Date) field.parse(ByteBuffer.wrap(bytes));
		Assert.assertEquals(DateUtil.toDate("20150101").getTime(), obj.getTime());
	}
}
