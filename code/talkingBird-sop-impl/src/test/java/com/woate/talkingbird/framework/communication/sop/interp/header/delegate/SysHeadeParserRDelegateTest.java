package com.woate.talkingbird.framework.communication.sop.interp.header.delegate;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.SysHeaderFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.interp.header.delegate.SysHeaderParserDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2IntegerConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Integer2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class SysHeadeParserRDelegateTest {
	@Test
	public void test1() throws TransformException{
		MetadataLoader loader = new MetadataLoader(){
			@Override
			public MetadataLoader load() {
				return this;
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				List<SOPMetadata> fields = new ArrayList<SOPMetadata>();
				SOPFixedField field1 = new SOPFixedFieldBuilder("SHJBCD").setAlias("数据包长度").setLength(2).setClazz(Integer.class).setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
				SOPFixedField field2 = new SOPFixedFieldBuilder("BAWMAC").setAlias("报文MAC").setLength(16).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
				fields.add(field1);
				fields.add(field2);
				return fields;
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
		};
		SysHeaderFormatterDelegate formatter = new SysHeaderFormatterDelegate(loader);
		SysHeaderParserDelegate parser = new SysHeaderParserDelegate(loader);
		SOPSession session = SOPSession.getSession();
		session.setAttribute("SHJBCD", 12);
		session.setAttribute("BAWMAC", "1234567890abcdef");
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		ByteBuffer buffer = formatter.format();
		System.out.println(Hex.toHexString(buffer.array()));
		buffer.rewind();
		session.attributes().clear();
		Assert.assertTrue(session.attributes().isEmpty());
		session.setAttribute(Dictionary.PACKET_TYPE, PacketType.REQ);
		parser.parse(buffer);
		System.out.println(session.attributes());
		Assert.assertEquals("1234567890abcdef", session.getAttribute("BAWMAC"));
		Assert.assertEquals(12, session.getAttribute("SHJBCD"));
		SOPSession.destory();
	}
}
