package com.woate.talkingbird.dto;

import java.util.Date;

public class OHJ220DTO {
	String prodId;
	Date startDate;
	Date endDate;
	Integer startAmount;
	Integer selectAmount;
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getStartAmount() {
		return startAmount;
	}
	public void setStartAmount(Integer startAmount) {
		this.startAmount = startAmount;
	}
	public Integer getSelectAmount() {
		return selectAmount;
	}
	public void setSelectAmount(Integer selectAmount) {
		this.selectAmount = selectAmount;
	}
	
}
