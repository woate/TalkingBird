package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.nio.ByteBuffer;
import java.util.Arrays;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPBeanImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPBeanWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2IntegerConverter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.Integer2ByteArrayConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class SOPBeanImplTest {

	@Test
	public void testFormat1() throws Exception {
		Bean1 bean = new Bean1("tom", 25);
		SOPBeanWriteable obj = new SOPBeanImpl("OBEAN0", "", "bean1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		obj.addField(field1);
		obj.addField(field2);
		byte[] bytes = obj.format(bean);
		System.out.println(Hex.toDisplayString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("064F4245414E3003746F6D0119"), bytes));
	}
	@Test
	public void testFormat2() throws Exception {
		Bean1 bean = new Bean1("��ķ", 30);
		SOPBeanWriteable obj = new SOPBeanImpl("OBEAN0", "", "bean1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		obj.addField(field1);
		obj.addField(field2);
		byte[] bytes = obj.format(bean);
		System.out.println(Hex.toDisplayString(bytes));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("064F4245414E3006E6B1A4E5A786011E"), bytes));
	}
	@Test(expected = TransformException.class)
	public void testFormat3() throws Exception {
		Bean1 bean = new Bean1("��ķ", 30);
		SOPBeanWriteable obj = new SOPBeanImpl("OBEAN0", "", "bean1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(2).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		obj.addField(field1);
		obj.addField(field2);
		obj.format(bean);
		Assert.fail();
	}
	@Test(expected = TransformException.class)
	public void testFormat4() throws Exception {
		Bean1 bean = new Bean1("��ķ", 30);
		SOPBeanWriteable obj = new SOPBeanImpl("OBEAN0", "", "bean1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(2).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		obj.addField(field1);
		obj.addField(field2);
		obj.format(bean);
		Assert.fail();
	}

	@Test
	public void testParse1() throws Exception {
		Bean1 bean = new Bean1("��ķ", 30);
		SOPBeanWriteable obj = new SOPBeanImpl("OBEAN0", "", "bean1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		obj.addField(field1);
		obj.addField(field2);
		byte[] bytes = obj.format(bean);
		System.out.println(Hex.toHexString(bytes));
		ByteBuffer input = ByteBuffer.wrap(bytes);
		Object o = obj.parse(input);
		System.out.println(o);
		Assert.assertEquals(bean, o);
	}
	@Test(expected = TransformException.class)
	public void testParse2() throws Exception {
		Bean1 bean = new Bean1("��ķ", 30);
		SOPBeanWriteable obj = new SOPBeanImpl("OBEAN0", "", "bean1", "UTF-8", Bean1.class);
		SOPVariableFieldWriteable field1 = new SOPVariableFieldBuilder("name").setDesc("����").setMinLength(1).setMaxLength(2).setClazz(String.class)
				.setFormatConverter(new String2ByteArrayConverter()).setParseConverter(new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		SOPVariableFieldWriteable field2 = new SOPVariableFieldBuilder("age").setDesc("����").setMinLength(1).setMaxLength(10).setClazz(Integer.class)
				.setFormatConverter(new Integer2ByteArrayConverter()).setParseConverter(new ByteArray2IntegerConverter()).setValueMode(ValueMode.USER).build();
		obj.addField(field1);
		obj.addField(field2);
		byte[] bytes = obj.format(bean);
		System.out.println(Hex.toHexString(bytes));
		ByteBuffer input = ByteBuffer.wrap(bytes);
		obj.parse(input);
		Assert.fail();
	}
}
