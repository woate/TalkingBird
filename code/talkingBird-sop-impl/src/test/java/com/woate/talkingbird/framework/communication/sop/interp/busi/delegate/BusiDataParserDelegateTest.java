package com.woate.talkingbird.framework.communication.sop.interp.busi.delegate;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.metadata.Metadata;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.interp.busi.SOPBusiData;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.BusiDataFormatterDelegate;
import com.woate.talkingbird.framework.communication.sop.interp.busi.delegate.BusiDataParserDelegate;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.Bean1;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPBeanImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPBeanWriteable;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
import com.woate.talkingbird.framework.core.AliasNameManager;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2StringConverter;
import com.woate.talkingbird.framework.core.converter.impl.String2ByteArrayConverter;
import com.woate.talkingbird.framework.util.Hex;

public class BusiDataParserDelegateTest {
	@Test
	public void testParse1() throws TransformException{
		SOPSession.destory();
		final List<SOPMetadata> list = new ArrayList<SOPMetadata>();
		SOPBeanWriteable obj0 = new SOPBeanImpl("O900b0","", "ChangeKey1","UTF-8", O900b1.class);
		SOPVariableFieldWriteable field0 = new SOPVariableFieldBuilder("name").setAlias("密钥名0").setMinLength(1).setMaxLength(12).setClazz(String.class).setFormatConverter(new String2ByteArrayConverter()).setParseConverter( new ByteArray2StringConverter()).setValueMode(ValueMode.USER).build();
		
		obj0.addField(field0);
		list.add(obj0);
		BusiDataMetadateLoader busiDataLoader = new BusiDataMetadateLoader(){
			@Override
			public BusiDataMetadateLoader load() {
				return this;
			}
			
			@Override
			public List<SOPMetadata> list(PacketType type) {
				return list;
			}

			@Override
			public String getTradeCode() {
				return "900b";
			}

			@Override
			public List<SOPMetadata> listHeader(PacketType type) {
				return new ArrayList<SOPMetadata>();
			}

			@Override
			public AliasNameManager getAliasNameManager() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public MainMetadataLoader getMainLoader() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public AliasNameManager getHeaderAliasNameManager() {
				// TODO 自动生成的方法存根
				return null;
			}

			@Override
			public String getHeadFileName() {
				// TODO 自动生成的方法存根
				return null;
			}
			
		};
		SOPSession session = SOPSession.getSession();
		SOPBusiWrapper busiWrapper = session.createBusiData();
		O900b1 o900b1 = new O900b1();
		o900b1.name = "this is test";
		busiWrapper.setBusidata("O900b0", o900b1);
		BusiDataFormatterDelegate formatter = new BusiDataFormatterDelegate("900b",busiDataLoader);
		ByteBuffer buffer = formatter.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Assert.assertTrue(Arrays.equals(Hex.fromHexString("064F39303062300C746869732069732074657374"), buffer.array()));
		buffer.rewind();
		//-----------以下进行解析------------------------------
		session.busiDatas().clear();
		BusiDataParserDelegate parser = new BusiDataParserDelegate("900b",busiDataLoader);
		session.createBusiData();
		parser.parse(buffer);
		O900b1 o900b1_new = session.peekFirstBusiData().getBusidata("O900b0");
		Assert.assertEquals("this is test", o900b1_new.name);
	}
}
