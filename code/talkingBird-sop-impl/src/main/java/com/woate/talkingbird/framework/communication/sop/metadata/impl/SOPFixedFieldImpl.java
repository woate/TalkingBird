/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPFixedFieldWriteable;
import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.util.ArraysUtil;
import com.woate.talkingbird.framework.util.Hex;

/**
 * 定长字段对象实现
 * 
 * @author liucheng 2014-12-16 下午03:05:29
 * 
 */
public class SOPFixedFieldImpl extends AbstractSOPFieldImpl implements SOPFixedFieldWriteable {

	public SOPFixedFieldImpl() {
		super();
	}
	
	@Override
	public void vildate() {
		super.vildate();
	}

	/**
	 * 长度
	 */
	protected int length;
	/**
	 * 是否进行右边填充
	 */
	protected boolean rightFill;

	@Override
	public int getMaxLength() {
		return length;
	}

	@Override
	public int getMinLength() {
		return length;
	}

	@Override
	public void setMaxLength(int length) {
		this.length = length;
	}

	@Override
	public void setMinLength(int length) {
		this.length = length;
	}

	@Override
	public byte[] format(Object input) throws TransformException {
		//如果字段为常量模式,则使用常量值
		if(valueMode == ValueMode.CONSTANT){
			input = value;
		}
		//如果字段为默认模式,同时输入为null,则使用默认值
		if(valueMode == ValueMode.DEFAULT && input == null){
			input = value;
			SOPSession.getSession().setAttribute(name, value);
		}
		byte[] msgs = null;
		if (input == null && clazz == String.class) {
			input = "";
		} else if (input == null && clazz == Integer.class) {
			input = 0;
		} else if (input == null && clazz == BigDecimal.class) {
			input = BigDecimal.ZERO;
		}
		if (formatConverter == null) {
			if (input instanceof byte[]) {
				msgs = (byte[]) input;
			} else {
				throw new TransformException("由于字段[" + name + "]["+ desc+"]数据不是byte[],同时未配置转换器(converter),导致无法处理[" + input + "]");
			}
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			if(clazz == String.class){
				if(encoding == null || encoding.isEmpty()){
					map.put(Converter.INPUT_ENCODING, "UTF-8");
				}else{				
					map.put(Converter.INPUT_ENCODING, encoding);
				}
			}else if(clazz == Date.class){
				map.put(Converter.DATE_FORMAT, "yyyyMMdd");
			}
			try {
				msgs = formatConverter.convert(input, map);				
			} catch (Exception e) {
				throw new TransformException("字段[" + name + "]["+ desc+"]转换时发生异常", e);
			}
		}
		if (msgs.length < length) {
			if (rightFill) {
				msgs = ArraysUtil.rfill(msgs, length, (byte) 0);
			} else {
				msgs = ArraysUtil.lfill(msgs, length, (byte) 0);
			}
		} else {
			//msgs = ArraysUtil.ltruncate(msgs, length);
		}
		return msgs;
	}

	/**
	 * 根据SOP报文的规定，只能存放0~0xFA的长度，0xFF作为续行符
	 */
	@Override
	public Object parse(ByteBuffer input) throws TransformException {
		int infactLen = input.remaining();
		if (infactLen < length) {
			throw new TransformException("字段[" + name + "]["+ desc+"]实际长度[" + infactLen + "]不足获取元信息定义长度[" + length + "]的数据！");
		}
		byte[] newbytes = new byte[length];
		input.get(newbytes);
		Object val = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			if(clazz == String.class){
				if(encoding == null || encoding.isEmpty()){
					map.put(Converter.INPUT_ENCODING, "UTF-8");
				}else{				
					map.put(Converter.INPUT_ENCODING, encoding);
				}
			}else if(clazz == Date.class){
				map.put(Converter.DATE_FORMAT, "yyyyMMdd");
			}
			val = parseConverter.convert(newbytes, map);				
		} catch (Exception e) {
			throw new TransformException("字段[" + name + "]["+ desc+"]转换时发生异常,["+ Hex.toHexString(newbytes) +"]", e);
		}
		return val;
	}

	@Override
	public int getMaxSOPLength() {
		return getMaxLength();
	}

	@Override
	public int getMinSOPLength() {
		return getMinLength();
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean isRightFill() {
		return rightFill;
	}
	public void setRightFill(boolean rightFill) {
		this.rightFill = rightFill;
	}
	
}
