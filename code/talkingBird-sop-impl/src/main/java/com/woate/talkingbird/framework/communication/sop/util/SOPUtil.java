/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.util;

import java.nio.ByteBuffer;

import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
/**
 * SOP工具类
 * @author liucheng
 *
 */
public class SOPUtil {
	/**
	 * 将字节数组拆分为SOP字节数组
	 * @param in
	 * @return
	 */
	public static byte[] sopWrap(byte[] in){
		ByteBuffer inBuffer = ByteBuffer.wrap(in);
		int len = in.length;
		int times = len / 0xFA + 1;
		len = len%SOPMetadata.MAX_LENGTH;
		ByteBuffer buffer = ByteBuffer.allocate(in.length + times);
		for (int i = 0; i < times; i++) {
			//如果最后就需要计算长度
			if(i + 1 == times){
				buffer.put((byte)len);
				byte[] bytes = new byte[len];
				inBuffer.get(bytes);
				buffer.put(bytes);
			}else{
				buffer.put((byte)SOPMetadata.CONTINUANCE);
				byte[] bytes = new byte[SOPMetadata.MAX_LENGTH];
				inBuffer.get(bytes);
				buffer.put(bytes);
			}
		}
		return buffer.array();
	}
}
