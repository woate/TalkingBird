/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.interp.busi.delegate;

import java.nio.ByteBuffer;
import java.util.List;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPParser;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPBean;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPForm;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
/**
 * 交易数据反编排代理类
 * @author liucheng
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class BusiDataParserDelegate implements SOPParser{
	/**
	 * 交易码
	 */
	String tradeCode;
	/**
	 * 业务数据加载器
	 */
	BusiDataMetadateLoader<SOPMetadata> loader;
	public BusiDataParserDelegate(String tradeCode, BusiDataMetadateLoader<SOPMetadata> loader) {
		this.tradeCode = tradeCode;
		this.loader = loader;
	}

	@Override
	public void parse(ByteBuffer input) throws TransformException {
		try {
			parse0(input);
		} catch (Exception e) {
			throw new TransformException("交易["+loader.getTradeCode()+"]在反编排时发生异常！",e);
		}
	}

	private void parse0(ByteBuffer input) throws TransformException {
		SOPSession session = SOPSession.getSession();
		PacketType packetType = session.getAttribute(Dictionary.PACKET_TYPE);
		List<SOPMetadata> parsers = (List<SOPMetadata>) this.loader.list(packetType);
		if(session.busiDatas().isEmpty()){
			throw new TransformException("接口["+tradeCode+"]解析数据准备失败,用户未设置交易数据!");
		}
		//该处的包装实在最开始解析时新建的
		SOPBusiWrapper busiWrapper = session.peekLastBusiData();
		for (SOPMetadata metadata : parsers) {
			if (metadata instanceof SOPBean) {
				SOPBean bean = (SOPBean) metadata;
				Object obj = bean.parse(input);
				busiWrapper.setBusidata(metadata.getName(), obj);
			} else if (metadata instanceof SOPForm) {
				SOPForm form = (SOPForm) metadata;
				Object list = form.parse(input);
				busiWrapper.setBusidata(metadata.getName(), list);
			} else if (metadata instanceof SOPFixedField) {
				SOPFixedField field = (SOPFixedField) metadata;
				Object obj= field.parse(input);
				busiWrapper.setBusidata(metadata.getName(), obj);
			} else if (metadata instanceof SOPVariableField) {
				SOPVariableField field = (SOPVariableField) metadata;
				Object obj= field.parse(input);
				busiWrapper.setBusidata(metadata.getName(), obj);
			}
		}
	}

}
