/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.interp.busi.delegate;


import java.nio.ByteBuffer;
import java.util.List;

import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPFormatter;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPBean;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPForm;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
/**
 * 业务数据编排代理类
 * @author liucheng
 * 2014-12-11 下午02:34:25
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class BusiDataFormatterDelegate implements SOPFormatter{
	/**
	 * 交易码
	 */
	String tradeCode;
	/**
	 * 业务数据加载器
	 */
	BusiDataMetadateLoader<SOPMetadata> loader;
	public BusiDataFormatterDelegate(String tradeCode, BusiDataMetadateLoader<SOPMetadata> loader) {
		this.tradeCode = tradeCode;
		this.loader = loader;
	}

	@Override
	public ByteBuffer format() throws TransformException {
		try {
			return format0();			
		} catch (Exception e) {
			throw new TransformException("交易["+loader.getTradeCode()+"]在编排时发生异常！",e);
		}
	}

	private ByteBuffer format0() throws TransformException {
		SOPSession session = SOPSession.getSession();
		PacketType packetType = session.getAttribute(Dictionary.PACKET_TYPE);
		List<SOPMetadata> formatters = (List<SOPMetadata>) this.loader.list(packetType);
		ByteBuffer buffer = null;
		int maxSOPLength = 0;
		// 计算出需要的缓存区大小
		SOPBusiWrapper busiWrapper = session.peekFirstBusiData();
		for (SOPMetadata metadata : formatters) {
			Object in = busiWrapper.getBusidata(metadata.getName());
			if (metadata instanceof SOPBean) {
				SOPBean bean = (SOPBean) metadata;
				maxSOPLength += bean.getMaxSOPLength();
			} else if (metadata instanceof SOPForm) {
				if(in == null){
					throw new TransformException("接口["+tradeCode+"]的交易数据定义存在["+ metadata.getName() +"]别名为["+ metadata.getAlias()+"],但实际输入数据却为Null!");
				}
				List list = (List) in;
				SOPForm form = (SOPForm) metadata;
				maxSOPLength += form.getMaxSOPLength(list.size());
			} else if (metadata instanceof SOPFixedField) {
				SOPFixedField field = (SOPFixedField) metadata;
				maxSOPLength += field.getMaxSOPLength();
			} else if (metadata instanceof SOPVariableField) {
				SOPVariableField field = (SOPVariableField) metadata;
				if(in == null && !field.isAllowNull() ){
					throw new TransformException("接口["+tradeCode+"]的交易数据定义存在["+ metadata.getName() +"]别名为["+ metadata.getAlias()+"],但实际输入数据却为Null!");
				}
				maxSOPLength += field.getMaxSOPLength();
			}
		}
		buffer = ByteBuffer.allocate(maxSOPLength);
		for (SOPMetadata metadata : formatters) {
			Object in = busiWrapper.getBusidata(metadata.getName());
			if (metadata instanceof SOPBean) {
				SOPBean bean = (SOPBean) metadata;
				byte[] bytes1 = bean.format(in);
				buffer.put(bytes1);
			} else if (metadata instanceof SOPForm) {
				List list = (List) in;
				SOPForm form = (SOPForm) metadata;
				byte[] bytes2 = (byte[]) form.format(list);
				buffer.put(bytes2);
			} else if (metadata instanceof SOPFixedField) {
				SOPFixedField field = (SOPFixedField) metadata;
				byte[] bytes1 = field.format(in);
				buffer.put(bytes1);
			} else if (metadata instanceof SOPVariableField) {
				SOPVariableField field = (SOPVariableField) metadata;
				byte[] bytes1 = field.format(in);
				buffer.put(bytes1);
			}
		}
		return buffer;
	}
}
