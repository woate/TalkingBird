/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.metadata.impl;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Map;

import com.woate.talkingbird.framework.communication.Formatter;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPBeanWriteable;

/**
 * SOP单条对象实现<br>
 * 
 * @author liucheng 2014-11-24 下午05:13:25
 * 
 */
public class SOPBeanImpl extends AbstractSOPObjectImpl implements SOPBeanWriteable {

	public SOPBeanImpl(String name, String alias, String desc, String encoding, Class<?> wrapClass) {
		super(name, alias, desc, encoding, wrapClass);
	}

	public byte[] format(Object input) throws TransformException {
		if (input == null) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的输入为空指针!");
		}
		if (wrapClass != input.getClass()) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的包装类[" + wrapClass.getName() + "]和实际数据包装类[" + input.getClass() + "]不一致");
		}
		init();
		ByteBuffer buffer = ByteBuffer.allocate(getMaxSOPLength());
		buffer.rewind();
		buffer.put((byte) 6);
		buffer.put(formatName());
		Map<String, Object> map = null;
		Class[] interfaces = input.getClass().getInterfaces();
		//如果输入类是Map接口,则使用Map访问
		for (Class interfaceClazz : interfaces) {
			if (interfaceClazz.isAssignableFrom(Map.class)) {
				map = (Map) input;
				break;
			}			
		}
		for (SOPVariableField field : fields) {
			Object val = null;
			if (map != null) {
				val = map.get(field.getName());
			} else {
				Method m = beanMethodCache.get(field.getName() + "@getter");
				if (m == null) {
					throw new TransformException("不存在元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]getter！");
				}
				try {
					val = m.invoke(input, new Object[0]);
				} catch (Exception e) {
					throw new TransformException("元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]getter执行时发生异常！");
				}
			}
			byte[] bytes = ((Formatter<Object, byte[]>) field).format(val);
			buffer.put(bytes);
		}
		int len = buffer.position();
		buffer.flip();
		byte[] newbytes = new byte[len];
		buffer.get(newbytes);
		return newbytes;
	}

	public Object parse(ByteBuffer input) throws TransformException {
		if (input == null) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的输入报文不能为空指针!");
		}
		if (input.limit() < getMinLength()) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的输入报文必须大于最小报文长度!");
		}
		int infactlen = input.get();
		if(infactlen != name.getBytes().length){
			throw new TransformException("元信息[" + name + "][" + desc + "]长度["+name.getBytes().length+"]与实际长度["+ infactlen+"]不一致!");
		}
		byte[] nameBytes = new byte[infactlen];
		input.get(nameBytes);
		String infactName = parseName(nameBytes);
		if (!name.equals(infactName)) {
			throw new TransformException("元信息[" + name + "][" + desc + "]描述的输入报文格式不正确,待解析对象元信息定义为[" + name + "],实际报文中的名称为[" + infactName + "]!");
		}
		init();
		Object wrapper = null;
		try {
			wrapper = wrapClass.newInstance();
		} catch (Exception e) {
			throw new TransformException(e.getCause());
		}
		Map<String, Object> map = null;
		//如果输入类是Map接口,则使用Map访问
		Class[] interfaces = wrapClass.getInterfaces();
		for (Class interfaceClazz : interfaces) {
			if (interfaceClazz.isAssignableFrom(Map.class)) {
				map = (Map) wrapper;
				break;
			}			
		}
		for (SOPVariableField field : fields) {
			Object val = field.parse(input);
			if (map != null) {
				map.put(field.getName(), val);
			} else {
				Method m = beanMethodCache.get(field.getName() + "@setter");
				if (m == null) {
					throw new TransformException("不存在元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]setter！");
				}
				try {
					m.invoke(wrapper, val);
				} catch (Exception e) {
					throw new TransformException("元信息[" + name + "][" + desc + "]描述的JavaBean[" + field.getName() + "]setter执行时发生异常！");
				}
			}
		}
		return wrapper;
	}

	@Override
	public int getMinLength() {
		return 0;
	}

	@Override
	public int getMaxSOPLength() {
		return getMinSOPLength() + getMaxLength();
	}

	@Override
	public int getMinSOPLength() {
		return 1 + name.length() + 1 + getMinLength();
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
