package com.woate.talkingbird.framework.core.converter;

import java.util.Map;

/**
 * 转换器接口<br>
 * 转换器为无状态的实现，且线程安全,即可以是单例的
 * @author liucheng
 * 2014-12-4 上午09:25:30
 *
 */
public interface Converter{
	/**
	 * 日期格式
	 */
	public static final String DATE_FORMAT = "DATE_FORMAT";
	/**
	 * 输入编码集
	 */
	public static final String INPUT_ENCODING = "INPUT_ENCODING";
	/**
	 * 输出编码集
	 */
	public static final String OUTPUT_ENCODING = "OUTPUT_ENCODING";
	/**
	 * 获转换器的相关信息
	 * @return 转换器的相关信息
	 */
	String getInfo();

	/**
	 * 转换为指定的目标格式
	 * @param in 输入
	 * @param params 转换器需要使用的参数
	 * @param <T> 类型
	 * @return 结果
	 */
	<T> T convert(Object in, Map<String, Object> params);	
}
