/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Map;

/**
 * 日期转换为字符串<br>
 * 输出的格式支持<br>
 * yyyy-MM-dd<br>
 * yyyy/MM/dd<br>
 * yyyyMMdd<br>
 * yyyyMMddHHmmss<br>
 * HHmmss<br>
 * HH:mm:ss<br>
 * 其他日期格式不支持
 * @author liucheng
 * 2014-12-5 上午08:46:32
 *
 */
public class Date2ByteArrayConverter extends Date2StringConverter {

	@Override
	public String getInfo() {
		return "com.brilliance.framework.communication.converter.impl.Date2ByteArrayConverter/1.0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convert(Object in, Map<String, Object> params) {
		String msg = super.convert(in, params);
		return (T)msg.getBytes();
	}
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
