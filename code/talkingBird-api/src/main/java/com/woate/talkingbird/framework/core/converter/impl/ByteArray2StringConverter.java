/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.nio.charset.Charset;
import java.util.Map;

/**
 * 把字节数组转换为字符串<br>
 * 
 * @author liucheng 2014-12-4 上午09:29:04
 * 
 */
public class ByteArray2StringConverter extends AbstractByteArrayConverter {

	@Override
	public String getInfo() {
		return "com.brilliance.framework.communication.converter.impl.ByteArray2StringConverter/1.0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convert(Object in, Map<String, Object> params) {
		super.vlidate(in, params);
		String encoding = (String)params.get(INPUT_ENCODING);
		Charset charset = Charset.forName(encoding);
		byte[] bytes = (byte[]) in;
		String msg = null;
		msg = new String(bytes, charset).trim();
		msg = (msg == null ? "" : msg);
		return (T)msg;
	}
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
