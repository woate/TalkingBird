/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader;

import com.woate.talkingbird.framework.core.AliasNameManager;



/**
 * SOP主加载器<br>
 * 在系统信息头加载器<br>
 * 在公共信息头加载器<br>
 * 在业务信息头加载器<br>
 * 在业务数据加载器中存放了多个编排器和反编排器<br>
 * 
 * @author woate 2014-12-16 下午04:10:01
 * 
 */
public interface MainMetadataLoader<T> extends Loader{
	/**
	 * 获取主加载器名称
	 * @return 加载器名称
	 */
	public String getName();
	/**
	 * 加载SOP元信息
	 * @return 元信息列表
	 */
	MainMetadataLoader<T> load();
	/**
	 * 获取系统信息头加载器
	 * @return 头加载器
	 */
	public MetadataLoader<T> getSysHeaderLoader();
	/**
	 * 获取公共信息头加载器
	 * @return 头加载器
	 */
	public MetadataLoader<T> getComHeaderLoader();
	/**
	 * 获取业务数据加载器数组
	 * @return 加载器数组
	 */
	public BusiDataMetadateLoader<T>[] getBusiDataLoaders();
	
	/**
	 * 获取别名管理器
	 * @return 别名管理器
	 */
	AliasNameManager getAliasNameManager();
	/**
	 * 获取根加载器
	 * @return 根加载器
	 */
	BootstrapMetadataLoader<T> getBoostrapLoader();
}
