/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Map;

/**
 * 将整数对象转换为十六进制的字节数组
 * @author liucheng
 * 2014-12-5 上午11:24:52
 *
 */
public class Integer2ByteArrayConverter extends AbstractConverter {

	@Override
	public String getInfo() {
		return "com.brilliance.framework.communication.converter.impl.Integer2ByteArrayConverter/1.0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convert(Object in, Map<String, Object> params) {
		vlidate(in, params);
		if(in.getClass() != Integer.class && in.getClass() != Integer.TYPE){
			throw new IllegalArgumentException("转换器输入数据["+ in +"]需要是[Integer],实际是["+in.getClass()+"]");
		}
		Integer i = (Integer)in;
		int len = 0;
		//获取需要的字节数组长度
		int tmp = i;
		while(true){
			if(tmp>>8 == 0){
				len++;
				break;
			}else{
				tmp = tmp>>8;
				len++;
			}
		}
		byte[] bytes = new byte[len];
		for (int j = 0; j < len; j++) {
			if(j+1 == len){				
				bytes[j] = (byte)(i & 0xff);
			}else{				
				bytes[j] = (byte)(i >> 8 & 0xff);
			}
		}
		return (T) bytes;
	}
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
