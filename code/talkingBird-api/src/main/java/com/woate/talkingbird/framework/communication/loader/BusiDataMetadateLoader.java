/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader;

import java.util.List;

import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 业务数据元信息加载器
 * @author woate
 * 2014-12-22 下午03:06:49
 *
 */
public interface BusiDataMetadateLoader<T> extends Loader,MetadataLoader<T>{
	/**
	 * 获取加载器名字，实际是头文件的文件名
	 * @return 头文件名称
	 */
	String getHeadFileName();
	
	BusiDataMetadateLoader<T> load();
	/**
	 * 获取交易码
	 * @return 交易码
	 */
	String getTradeCode();
	/**
	 * 获取元信息
	 * @param type 包类型
	 * @return 信息列表
	 */
	List<T> list(PacketType type);
	/**
	 * 获取加载后的元信息列表，顺序按照定义顺序
	 * @return 信息列表
	 */
	List<T> listHeader(PacketType type);
	
	/**
	 * 获取别名管理器
	 * @return 别名管理器
	 */
	AliasNameManager getAliasNameManager();
	/**
	 * 获取头信息别名管理器
	 * @return 别名管理器
	 */
	AliasNameManager getHeaderAliasNameManager();
	
	/**
	 * 获取主加载器
	 * @return 加载器
	 */
	MainMetadataLoader<T> getMainLoader();
}
