/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.util;

import java.sql.Timestamp;
import java.text.*;
import java.util.*;

public class DateUtil{
	/**
	 * 精确到毫秒的日期格式 yyyy-MM-dd HH:mm:ss,SSS
	 */
	public static final String MILLI_FORMAT1  = "yyyy-MM-dd HH:mm:ss,SSS";
	/**
	 * 精确到毫秒的日期格式 yyyy/MM/dd HH:mm:ss,SSS
	 */
	public static final String MILLI_FORMAT2  = "yyyy/MM/dd HH:mm:ss,SSS";
	/**
	 * 精确到毫秒的日期格式 HH:mm:ss,SSS
	 */
	public static final String MILLI_FORMAT3  = "HH:mm:ss,SSS";
	/**
	 * 精确到秒的日期格式 yyyy-MM-dd HH:mm:ss
	 */
	public static final String SECOND_FORMAT1 = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 精确到秒的日期格式 yyyy/MM/dd HH:mm:ss
	 */
	public static final String SECOND_FORMAT2 = "yyyy/MM/dd HH:mm:ss";
	/**
	 * 精确到分的日期格式 yyyy-MM-dd HH:mm
	 */
	public static final String MINUTE_FORMAT1 = "yyyy-MM-dd HH:mm";
	/**
	 * 精确到分的日期格式 yyyy/MM/dd HH:mm
	 */
	public static final String MINUTE_FORMAT2 = "yyyy/MM/dd HH:mm";
	/**
	 * 精确到天的日期格式 yyyy-MM-dd
	 */
	public static final String DAY_FORMAT1    = "yyyy-MM-dd";
	/**
	 * 精确到天的日期格式 yyyy/MM/dd
	 */
	public static final String DAY_FORMAT2    = "yyyy/MM/dd";
	/**
	 * 能够用于文件命名的日期 yyyy-MM-dd_HH-mm-ss
	 */
	public static final String FILE_FORMAT1   = "yyyy-MM-dd_HH-mm-ss";
	/**
	 * 能够用于文件命名的日期 yyyyMMddHHmmss
	 */
	public static final String FILE_FORMAT2   = "yyyyMMddHHmmss";
	/**
	 * 能够用于文件命名的日期 yyyyMMdd
	 */
	public static final String FILE_FORMAT3   = "yyyyMMdd";
	/**
	 * 能够用于文件命名的日期 HH:mm:ss
	 */
	public static final String FILE_FORMAT5   = "HH:mm:ss";
	/**
	 * 能够用于文件命名的日期 HHmmss
	 */
	public static final String FILE_FORMAT4   = "HHmmss";
	
	/**
	 * 中文日期格式 yyyy年MM月dd日 HH时mm分ss秒
	 */
	public static final String CHINESE_FORMAT1 = "yyyy年MM月dd日 HH时mm分ss秒";
	/**
	 * 中文日期格式 yyyy年MM月dd日
	 */
	public static final String CHINESE_FORMAT2 = "yyyy年MM月dd日";
	/**
	 * 中文时间格式 HH时mm分ss秒
	 */
	public static final String CHINESE_FORMAT3 = "HH时mm分ss秒";
	
	private DateUtil() {
		
	}

	/**
	 * 将时间戳对象转换为日期对象
	 * @param timestamp 时间戳对象
	 * @return 日期对象
	 */
	public static Date toDate(Timestamp timestamp) {
		return new Date(timestamp.getTime());
	}
	/**
	 * 将日历对象转换为日期对象
	 * @param cal 日历对象
	 * @return 日期对象
	 */
	public static Date toDate(Calendar cal) {
		return cal.getTime();
	}
	/**
	 * 将日期对象转换为日历对象
	 * @param date 日期对象
	 * @return 日历对象
	 */
	public static Calendar toCalendar(Date date) {
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return gc;
	}
	/**
	 * 将时间戳对象转换为日历对象
	 * @param timestamp 时间戳对象
	 * @return 日历对象
	 */
	public static Calendar toCalendar(Timestamp timestamp) {
		return toCalendar(toDate(timestamp));
	}
	/**
	 * 将日期对象转换为时间戳对象
	 * @param date 日期对象
	 * @return 时间戳对象
	 */
	public static Timestamp toTimestamp(Date date) {
		Timestamp timestamp = new Timestamp(date.getTime());
		return timestamp;
	}
	/**
	 * 将日历对象转换为时间戳对象
	 * @param cal 日历对象
	 * @return 时间戳对象
	 */
	public static Timestamp toTimestamp(Calendar cal) {
		return toTimestamp(toDate(cal));
	}

	/**
	 * 获取格里高利历日历对象
	 * @return 日历对象
	 */
	public static Calendar getCalendar() {
		return GregorianCalendar.getInstance();
	}


	/**
	 * 判断当前日期是否为闰年
	 * @return  如果为闰年为真，反之为假
	 */
	public static boolean isLeapYear() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		return isLeapYear(year);
	}

	/**
	 * 判断输入的年份是否为闰年
	 * @param year 年份
	 * @return  如果为闰年为真，反之为假
	 */
	public static boolean isLeapYear(int year) {
		if ((year % 400) == 0)
			return true;
		else if ((year % 4) == 0) {
			if ((year % 100) == 0)
				return false;
			else
				return true;
		} else
			return false;
	}
	
	/**
	 * 判断输入的日期是否为闰年
	 * @param date 输入的日期
	 * @return 如果为闰年为真，反之为假
	 */
	public static boolean isLeapYear(java.util.Date date) {
		return isLeapYear(toCalendar(date));
	}

	/**
	 * 判断输入的日期是否为闰年
	 * @param gc 输入的日期
	 * @return 如果为闰年为真，反之为假
	 */
	public static boolean isLeapYear(Calendar gc) {
		int year = gc.get(Calendar.YEAR);
		return isLeapYear(year);
	}
	/**
	 * 获取当前时间点的上周同一时间点日期对象
	 * @param date 给定时间对象
	 * @return 日期对象
	 */
	public static Date getPreviousWeekDay(Date date) {
		return getPreviousWeekDay(toCalendar(date));
	}
	
	/**
	 * 获取当前时间点的上周同一时间点日期对象
	 * @param gc 给定时间对象
	 * @return 日期对象
	 */
	public static Date getPreviousWeekDay(Calendar gc) {
		{
			switch (gc.get(Calendar.DAY_OF_WEEK)) {
			case (Calendar.MONDAY    ):
				gc.add(Calendar.DATE, -3);
				break;
			case (Calendar.SUNDAY    ):
				gc.add(Calendar.DATE, -2);
				break;
			default:
				gc.add(Calendar.DATE, -1);
				break;
			}
			return gc.getTime();
		}
	}

	/**
	 * 获取当前时间点的上周同一时间点日历对象
	 * @param cal 给定日历对象
	 * @return 日历对象
	 */
	public static Calendar getNextWeekDay(Calendar cal) {
		switch (cal.get(Calendar.DAY_OF_WEEK)) {
			case (Calendar.FRIDAY    ):
				cal.add(Calendar.DATE, 3);
				break;
			case (Calendar.SATURDAY    ):
				cal.add(Calendar.DATE, 2);
				break;
			default:
				cal.add(Calendar.DATE, 1);
				break;
			}
		return cal;
	}
	
	/**
	 * 获取当前时间点的上周同一时间点日期对象
	 * @param date 给定日期对象
	 * @return 日期对象
	 */
	public static Date getNextWeekDay(Date date) {
		return getNextWeekDay(toCalendar(date)).getTime();
	}


	/**
	 * 获取给定日期的下个月的最后天的日期
	 * @param date  输入的日期对象
	 * @return 下个月的最后天的日期??
	 */
	public static Date getLastDayOfNextMonth(Date date) {
		return getLastDayOfNextMonth(toCalendar(date)).getTime();
	}
	
	/**
	 * 获取给定日期的下个月的最后天的日期
	 * @param cal  输入的日历对象
	 * @return 下个月的最后天的日期??
	 */
	public static Calendar getLastDayOfNextMonth(Calendar cal) {
		cal.setTime(getNextMonth(cal).getTime());
		cal.setTime(getLastDayOfMonth(cal).getTime());
		return cal;
	}
	/**
	 * 获取给定日期的下周的最后天的日期
	 * @param date  输入的日期对象
	 * @return 下周的最后天的日期??
	 */
	public static Date getLastDayOfNextWeek(Date date) {
		return getLastDayOfWeek(toCalendar(date)).getTime();
	}
	/**
	 * 获取给定日期的下周的最后天的日期
	 * @param cal  输入的日历对象
	 * @return 下周的最后天的日期??
	 */
	public static Calendar getLastDayOfNextWeek(Calendar cal) {
		cal.setTime(getNextWeek(cal.getTime()));
		cal.setTime(getLastDayOfWeek(cal.getTime()));
		return cal;
	}

	/**
	 * 获取给定日期的下个月的第一天的日期
	 * @param date  输入的日期对象
	 * @return 下个月的第一天的日期??
	 */
	public static Date getFirstDayOfNextMonth(Date date) {
		return getFirstDayOfNextMonth(toCalendar(date)).getTime();
	}

	/**
	 * 获取给定日期的下个月的第一天的日期
	 * @param gc  输入的日历对象
	 * @return 下个月的第一天的日期??
	 */
	public static Calendar getFirstDayOfNextMonth(Calendar gc) {
		gc.setTime(DateUtil.getNextMonth(gc.getTime()));
		gc.setTime(DateUtil.getFirstDayOfMonth(gc.getTime()));
		return gc;
	}

	/**
	 * 获取给定日期的下个星期的第一天的日期
	 * @param date  输入的日期对象
	 * @return 下个星期的第一天的日期??
	 */
	public static Date getFirstDayOfNextWeek(Date date) {
		return getFirstDayOfNextWeek(toCalendar(date)).getTime();
	}
	/**
	 * 获取给定日期的下个星期的第一天的日期
	 * @param gc  输入的日历对象
	 * @return 下个星期的第一天的日期
	 */
	public static Calendar getFirstDayOfNextWeek(Calendar gc) {
		gc.setTime(getNextWeek(gc.getTime()));
		gc.setTime(getFirstDayOfWeek(gc.getTime()));
		return gc;
	}

	/**
	 * 获取给定日期的下个月日期
	 * @param date 输入的日期对象
	 * @return 下个月日期日期对象
	 */
	public static Date getNextMonth(Date date) {
		return getNextMonth(toCalendar(date)).getTime();
	}

	/**
	 * 获取给定日期的下个月日期
	 * @param gc 输入的日历对象
	 * @return 下个月日期日历对象
	 */
	public static java.util.Calendar getNextMonth(java.util.Calendar gc) {
		gc.add(Calendar.MONTH, 1);
		return gc;
	}

	/**
	 * 获取给定日期的明天日期
	 * @param date 输入的日期对象
	 * @return 明天日期对象
	 */
	public static Date getNextDay(Date date) {
		return getNextDay(toCalendar(date)).getTime();
	}

	/**
	 * 获取给定日期的明天日期
	 * @param gc 输入的日历对象
	 * @return 明天日历对象
	 */
	public static java.util.Calendar getNextDay(java.util.Calendar gc) {
		gc.add(Calendar.DATE, 1);
		return gc;
	}

	/**
	 * 获取给定日期的昨天日期
	 * @param gc 输入的日历对象
	 * @return 明天日历对象
	 */
	public static java.util.Calendar getPreviousDay(java.util.Calendar gc) {
		gc.add(Calendar.DATE, -1);
		return gc;
	}
	/**
	 * 获取给定日期的昨天日期
	 * @param date 输入的日历对象
	 * @return 明天日历对象
	 */
	public static Date getPreviousDay(Date date) {
		return getPreviousDay(toCalendar(date)).getTime();
	}
	
	/**
	 * 获取给定日期的下周日期
	 * @param date 输入的日期对象
	 * @return 下周当前时间点日期对象
	 */
	public static Date getNextWeek(Date date) {
		return getNextWeek(toCalendar(date)).getTime();
	}

	/**
	 * 获取给定日期的下周日期
	 * @param gc 输入的日历对象
	 * @return 下周当前时间点日历对象
	 */
	public static java.util.Calendar getNextWeek(java.util.Calendar gc) {
		gc.add(Calendar.DATE, 7);
		return gc;
	}

	/**
	 * 计算给定两个日期间的月份数
	 * @param begingc 起始日期
	 * @param endgc 结束日期
	 * @return 月份数
	 */
	public static int subMonth(java.util.Calendar begingc, java.util.Calendar endgc) {
		int beginYear = begingc.get(Calendar.YEAR);
		int beginMonth = begingc.get(Calendar.MONTH);
		int endYear = endgc.get(Calendar.YEAR);
		int endMonth = endgc.get(Calendar.MONTH);
		return (endYear - beginYear) * 12 + (endMonth - beginMonth);
	}

	/**
	 * 获取给定日期的所在周的最后一天
	 * @param date 给定日期的日期对象
	 * @return 所在周的最后一天
	 */
	public static Date getLastDayOfWeek(Date date) {
		return getLastDayOfWeek(toCalendar(date)).getTime();
	}
	/**
	 * 获取给定日期的所在周的最后一天
	 * @param gc 给定日期的日历
	 * @return 所在周的最后一天
	 */
	public static java.util.Calendar getLastDayOfWeek(java.util.Calendar gc) {
		switch (gc.get(Calendar.DAY_OF_WEEK)) {
			case (Calendar.SUNDAY  ):
				gc.add(Calendar.DATE, 6);
			break;
			case (Calendar.MONDAY  ):
				gc.add(Calendar.DATE, 5);
			break;
			case (Calendar.TUESDAY  ):
				gc.add(Calendar.DATE, 4);
			break;
			case (Calendar.WEDNESDAY  ):
				gc.add(Calendar.DATE, 3);
			break;
			case (Calendar.THURSDAY  ):
				gc.add(Calendar.DATE, 2);
			break;
			case (Calendar.FRIDAY  ):
				gc.add(Calendar.DATE, 1);
			break;
			case (Calendar.SATURDAY  ):
				gc.add(Calendar.DATE, 0);
			break;
		}
		return gc;
	}

	/**
	 * 获取给定日期的所在周的第一天
	 * @param date 给定日期的日期对象
	 * @return 所在周的第一天
	 */
	public static Date getFirstDayOfWeek(Date date) {
		return getFirstDayOfWeek(toCalendar(date)).getTime();
	}

	/**
	 * 获取给定日期的所在周的第一天
	 * @param gc 给定日期的日历
	 * @return 所在周的第一天
	 */
	public static java.util.Calendar getFirstDayOfWeek(java.util.Calendar gc) {
		switch (gc.get(Calendar.DAY_OF_WEEK)) {
		case (Calendar.SUNDAY  ):
			gc.add(Calendar.DATE, 0);
			break;
		case (Calendar.MONDAY  ):
			gc.add(Calendar.DATE, -1);
			break;
		case (Calendar.TUESDAY  ):
			gc.add(Calendar.DATE, -2);
			break;
		case (Calendar.WEDNESDAY  ):
			gc.add(Calendar.DATE, -3);
			break;
		case (Calendar.THURSDAY  ):
			gc.add(Calendar.DATE, -4);
			break;
		case (Calendar.FRIDAY  ):
			gc.add(Calendar.DATE, -5);
			break;
		case (Calendar.SATURDAY  ):
			gc.add(Calendar.DATE, -6);
			break;
		}
		return gc;
	}
	/**
	 * 获取月份的最后一天
	 * @param date 日期对象
	 * @return 月份的最后一天的最后一天
	 */
	public static Date getLastDayOfMonth(Date date) {
		return getLastDayOfMonth(toCalendar(date)).getTime();
	}

	/**
	 * 获取月份的最后一天
	 * @param gc 日历对象
	 * @return 月份的最后一天的最后一天
	 */
	public static java.util.Calendar getLastDayOfMonth(java.util.Calendar gc) {
		switch (gc.get(Calendar.MONTH)) {
		case 0:
			gc.set(Calendar.DAY_OF_MONTH, 31);
			break;
		case 1:
			gc.set(Calendar.DAY_OF_MONTH, 28);
			break;
		case 2:
			gc.set(Calendar.DAY_OF_MONTH, 31);
			break;
		case 3:
			gc.set(Calendar.DAY_OF_MONTH, 30);
			break;
		case 4:
			gc.set(Calendar.DAY_OF_MONTH, 31);
			break;
		case 5:
			gc.set(Calendar.DAY_OF_MONTH, 30);
			break;
		case 6:
			gc.set(Calendar.DAY_OF_MONTH, 31);
			break;
		case 7:
			gc.set(Calendar.DAY_OF_MONTH, 31);
			break;
		case 8:
			gc.set(Calendar.DAY_OF_MONTH, 30);
			break;
		case 9:
			gc.set(Calendar.DAY_OF_MONTH, 31);
			break;
		case 10:
			gc.set(Calendar.DAY_OF_MONTH, 30);
			break;
		case 11:
			gc.set(Calendar.DAY_OF_MONTH, 31);
			break;
		}
		if ((gc.get(Calendar.MONTH) == Calendar.FEBRUARY) && (isLeapYear(gc.get(Calendar.YEAR)))) {
			gc.set(Calendar.DAY_OF_MONTH, 29);
		}
		return gc;
	}

	
	/**
	 * 获取给定日期的所在月份的第一天的当前时间点
	 * @param cal 日历对象
	 * @return 月份的第一天当前时间点
	 */
	public static Date getFirstDayOfMonth(Calendar cal) {
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}
	/**
	 * 获取给定日期的所在月份的第一天的当前时间点
	 * @param date 日历对象
	 * @return 月份的第一天当前时间点
	 */
	public static Date getFirstDayOfMonth(Date date) {
		return getFirstDayOfMonth(toCalendar(date));
	}

	/**
	 * 获取两个日期间的天数
	 * @param startDate 起始日期
	 * @param endDate 终止日期
	 * @return 天数
	 */
	public static double getDayAmount(Date startDate, Date endDate) {
		return ((endDate.getTime() - startDate.getTime()) / (double) (1000 * 60 * 60 * 24)) + 1;
	}

	/**
	 * 在输入的oldDate日期基础上获得addDays天后的日期对象
	 * @param oldDate 输入的日期
	 * @param addDays 几天后
	 * @return 日期对象
	 */
	public static Date addDay(Date oldDate, int addDays) {
		Calendar calendar = toCalendar(oldDate);
		calendar.add(Calendar.DATE, addDays);
		return calendar.getTime();
	}


	
	/**
	 * 获取输入的日期时间，获取对应的掩码格式
	 * @param strDate 时间或日期字符串
	 * @return 掩码格式，例如 
	 * yyyy-MM-dd 
	 * yyyy/MM/dd 
	 * yyyyMMdd
	 * yyyy年MM月dd日
	 * yyyy-MM-dd-HH-mm-ss
	 * yyyyMMddHHmmss
	 * yyyy-MM-dd HH:mm:ss
	 * yyyy/MM/dd HH:mm:ss
	 * yyyy年MM月dd日 HH时mm分ss秒
	 * HH时mm分ss秒
	 * HH:mm:ss
	 */
	public static String getPartten(String strDate)
	{
		strDate = strDate.trim();
		if(strDate!=null) {
			if(strDate.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
				return "yyyy-MM-dd";
			}else if (strDate.matches("^\\d{4}\\d{2}\\d{2}$")) {
				return "yyyyMMdd";
			}else if (strDate.matches("^\\d{4}/\\d{2}/\\d{2}$")) {
				return "yyyy/MM/dd";
			}else if (strDate.matches("^\\d{4}年\\d{2}月\\d{2}日$")) {
				return "yyyy年MM月dd日";
			}else if (strDate.matches("^\\d{4}-\\d{2}-\\d{2}-\\d{2}-\\d{2}-\\d{2}$")) {				
				return "yyyy-MM-dd-HH-mm-ss";
			}else if (strDate.matches("^\\d{4}\\d{2}\\d{2}\\d{2}\\d{2}\\d{2}$")) {				
				return "yyyyMMddHHmmss";
			}else if(strDate.matches("^\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}\\.0$")){
				return "yyyy-MM-dd HH:mm:ss";	
			}else if (strDate.matches("^\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}$")) {				
				return "yyyy-MM-dd HH:mm:ss";
			}else if (strDate.matches("^\\d{4}/\\d{2}/\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}$")) {				
				return "yyyy/MM/dd HH:mm:ss";
			}else if (strDate.matches("^\\d{4}年\\d{2}月\\d{2}日\\s+\\d{2}时\\d{2}分\\d{2}秒$")) {				
				return "yyyy年MM月dd日 HH时mm分ss秒";
			}else if (strDate.matches("^\\d{2}\\d{2}\\d{2}$")) {
				return "HHmmss";
			}else if (strDate.matches("^\\d{2}:\\d{2}:\\d{2}$")) {
				return "HH:mm:ss";
			}else if (strDate.matches("^\\d{2}时\\d{2}分\\d{2}秒$")) {
				return "HH时mm分ss秒";
			}
		}
		return null;
	}
	/**
	 * 将符合日期格式的字符串转化为日期对象
	 * <p>
	 * 符合这样日期格式的
	 * yyyyMMdd
	 * yyyy/MM/dd 
	 * yyyy-MM-dd 
	 * yyyy年MM月dd日
	 * yyyyMMddHHmmss
	 * yyyy/MM/dd HH:mm:ss 
	 * yyyy-MM-dd HH:mm:ss 
	 * yyyy年MM月dd日 HH时mm分ss秒
	 * HH时mm分ss秒 
	 * HH:mm:ss 
	 * </p>
	 * @param dateStr 日期字符串
	 * @return 日期对象
	 */
	public static Date toDate(String dateStr){
		String formatStr = getPartten(dateStr);
		if ( formatStr == null) {
			throw new IllegalArgumentException("无效的日期格式");
		}else if (formatStr.equals("yyyy年MM月dd日 HH时mm分ss秒") || formatStr.equals("yyyy年MM月dd日") || formatStr.equals("HH时mm分ss秒")) {
			dateStr.replaceAll("年|月|日|时|分|秒", "");
			formatStr = getPartten(dateStr);
		}
		try{
			SimpleDateFormat format = new SimpleDateFormat(formatStr);
			return format.parse(dateStr);
		}catch(Exception e){
			return null;
		}
	}
	
	/**
	 * 以数字方式获取一个Date对象，例如 toDate(2011,12,31,12,24,31) 可以获取时间2011/12/31 12:24:31
	 * @param year 年
	 * @param month 月
	 * @param date 日
	 * @param hour 时
	 * @param min 分
	 * @param sec 秒
	 * @return  日期对象
	 */
	public static Date toDate(int year, int month, int date, int hour, int min, int sec) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, date, hour, min, sec);
		return calendar.getTime();
	}

	/**
	 * 以数字方式获取一个Date对象，例如 toDate(2011,12,31) 可以获取时间2011/12/31 0:0:0
	 * @param year 年
	 * @param month 月
	 * @param date 日
	 * @return 日期对象
	 */
	public static Date toDate(int year, int month, int date) {
		return toDate(year, month, date, 0, 0, 0);
	}
	
	public static enum DateConstants{
		/**
		 * 年
		 */
		YEAR,
		/**
		 * 月
		 */
		MONTH,
		/**
		 * 日
		 */
		DAY,
		/**
		 * 小时
		 */
		HOUR,
		/**
		 * 分钟
		 */
		MIN,
		/**
		 * 秒
		 */
		SECOND
	}
	/**
	 * 根据type的值，获取日期指定的属性，例如2011/12/31 12:21:30 传入DAY，可获得31
	 * @param date 日期实例
	 * @param type 取值有 YEAR 
	 * @see DateConstants
	 * @return 日期或时间对应的数字
	 */
	public static int getDateAttr(java.util.Date date, DateConstants type) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (DateConstants.YEAR == type) {
			return calendar.get(Calendar.YEAR);
		} else if (DateConstants.MONTH == type) {
			return calendar.get(Calendar.MONTH) + 1;
		} else if (DateConstants.DAY == type) {
			return calendar.get(Calendar.DAY_OF_MONTH);
		} else if (DateConstants.HOUR == type) {
			return calendar.get(Calendar.HOUR);
		} else if (DateConstants.MIN == type) {
			return calendar.get(Calendar.MINUTE);
		} else if (DateConstants.SECOND == type) {
			return calendar.get(Calendar.SECOND);
		}
		return -1;
	}

	/**
	 * 
	 * 根据type的值，获取日期指定的属性，例如2011/12/31 12:21:30 传入DAY，可获得31
	 * @param date 日期的long秒
	 * @param type 取值有 YEAR 
	 * @see DateConstants
	 * @return 日期或时间对应的数字
	 */
	public static int getDateAttr(long date, DateConstants type) {
		return getDateAttr(new java.util.Date(date), type);
	}
	

	/**
	 * 获取两个日期间的天数
	 * @param fst 起始日期
	 * @param lst 截止日期
	 * @return 天数
	 */
	public static long DateBetween(Timestamp fst, Timestamp lst) {
		return DateBetween(toDate(fst), toDate(lst));
		
	}
	
	/**
	 * 获取两个日期间的天数
	 * @param fst 起始日期
	 * @param lst 截止日期
	 * @return 天数
	 */
	public static long DateBetween(Calendar fst, Calendar lst) {
		return DateBetween(toDate(fst), toDate(lst));
	}
	
	/**
	 * 获取两个日期间的天数
	 * @param fst 起始日期
	 * @param lst 截止日期
	 * @return 天数
	 */
	public static Long DateBetween(Date fst, Date lst) {
		return (lst.getTime() - fst.getTime()) / 86400000; 
	}
	/**
	 * 获取两个日期间的月份数
	 * @param date_fst 起始日期
	 * @param date_lst 截止日期
	 * @return 月份数
	 */
	public static int MonthBetween(Timestamp date_fst, Timestamp date_lst) {
		return MonthBetween(toCalendar(date_fst),toCalendar(date_lst));
	}
	/**
	 * 获取两个日期间的月份数
	 * @param date_fst 起始日期
	 * @param date_lst 截止日期
	 * @return 月份数
	 */
	public static int MonthBetween(Date date_fst, Date date_lst) {
		return MonthBetween(toCalendar(date_fst),toCalendar(date_lst));
	}
	/**
	 * 获取两个日期间的月份数
	 * @param cld_fst 起始日期
	 * @param cld_lst 截止日期
	 * @return 月份数
	 */
	public static int MonthBetween(Calendar cld_fst, Calendar cld_lst) {
		int nYear, nMonth, nDate;
		nYear = cld_lst.get(Calendar.YEAR) - cld_fst.get(Calendar.YEAR);
		nMonth = cld_lst.get(Calendar.MONTH) - cld_fst.get(Calendar.MONTH);
		nDate = cld_lst.get(Calendar.DATE) - cld_fst.get(Calendar.DATE);
		return nYear * 12 + nMonth + (nDate >= 0 ? 0 : -1);
	}
	
	/**
	 * 将输入的日期对象根据日期掩码方式输出为字符串
	 * @param cal 输入的日历
	 * @param pattern 日期掩码
	 * @return 字符串
	 */
	public static String toString(java.util.Calendar cal, String pattern) {
		return toString(cal.getTime(), pattern);
	}
	/**
	 * 将输入的时间戳对象根据日期掩码方式输出为字符串
	 * @param timestamp 时间戳对象
	 * @param pattern 日期掩码
	 * @return 字符串
	 */
	public static String toString(Timestamp timestamp, String pattern) {
		return toString(toDate(timestamp), pattern);
	}
	/**
	 * 将输入的日期对象根据日期掩码方式输出为字符串
	 * @param date 输入的日期
	 * @param pattern 日期掩码
	 * @return 字符串
	 */
	public static String toString(java.util.Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat();
		String str = null;
		sdf.applyPattern(pattern);
		str = sdf.format(date);
		return str;
	}
	
	/**
	 * 将时间戳对象输出为yyyy-MM-dd HH:mm:ss,SSS 字符串 格式为 2011-09-01 09:09:09,567
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat1(Timestamp timestamp){
		return toStringByMilliFormat1(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy-MM-dd HH:mm:ss,SSS 字符串 格式为 2011-09-01 09:09:09,567
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat1(Date date){
		return toStringByMilliFormat1(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy-MM-dd HH:mm:ss,SSS 字符串 格式为 2011-09-01 09:09:09,567
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat1(Calendar cal){
		return toString(cal, MILLI_FORMAT1);
	}
	/**
	 * 将时间戳对象输出为yyyy/MM/dd HH:mm:ss,SSS 字符串 格式为 2011/09/01 09:09:09,567
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat2(Timestamp timestamp){
		return toStringByMilliFormat2(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy/MM/dd HH:mm:ss,SSS 字符串 格式为 2011/09/01 09:09:09,567
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat2(Date date){
		return toStringByMilliFormat2(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy/MM/dd HH:mm:ss,SSS 字符串 格式为 2011/09/01 09:09:09,567
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat2(Calendar cal){
		return toString(cal, MILLI_FORMAT2);
	}
	/**
	 * 将日期对象输出为HH:mm:ss,SSS 字符串 格式为 09:09:09,567
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat3(Date date){
		return toStringByMilliFormat3(toCalendar(date));
	}
	/**
	 * 将日历对象输出为HH:mm:ss,SSS 字符串 格式为 09:09:09,567
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMilliFormat3(Calendar cal){
		return toString(cal, MILLI_FORMAT3);
	}
	/**
	 * 将时间戳对象输出为yyyy-MM-dd HH:mm:ss 字符串 格式为 2011-09-01 09:09:09
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringBySecondFormat1(Timestamp timestamp){
		return toStringBySecondFormat1(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy-MM-dd HH:mm:ss 字符串 格式为 2011-09-01 09:09:09
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringBySecondFormat1(Date date){
		return toStringBySecondFormat1(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy-MM-dd HH:mm:ss 字符串 格式为 2011-09-01 09:09:09
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringBySecondFormat1(Calendar cal){
		return toString(cal, SECOND_FORMAT1);
	}
	/**
	 * 将时间戳对象输出为yyyy/MM/dd HH:mm:ss 字符串 格式为 2011/09/01 09:09:09
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringBySecondFormat2(Timestamp timestamp){
		return toStringBySecondFormat2(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy/MM/dd HH:mm:ss 字符串 格式为 2011/09/01 09:09:09
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringBySecondFormat2(Date date){
		return toStringBySecondFormat2(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy/MM/dd HH:mm:ss 字符串 格式为 2011/09/01 09:09:09
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringBySecondFormat2(Calendar cal){
		return toString(cal, SECOND_FORMAT2);
	}
	/**
	 * 将时间戳对象输出为yyyy-MM-dd HH:mm 字符串 格式为 2011-09-01 09:09
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMinuteFormat1(Timestamp timestamp){
		return toStringByMinuteFormat1(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy-MM-dd HH:mm 字符串 格式为 2011-09-01 09:09
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMinuteFormat1(Date date){
		return toStringByMinuteFormat1(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy-MM-dd HH:mm 字符串 格式为 2011-09-01 09:09
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMinuteFormat1(Calendar cal){
		return toString(cal, MINUTE_FORMAT1);
	}
	/**
	 * 将时间戳对象输出为yyyy/MM/dd HH:mm 字符串 格式为 2011/09/01 09:09
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMinuteFormat2(Timestamp timestamp){
		return toStringByMinuteFormat2(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy/MM/dd HH:mm 字符串 格式为 2011/09/01 09:09
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMinuteFormat2(Date date){
		return toStringByMinuteFormat2(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy/MM/dd HH:mm 字符串 格式为 2011/09/01 09:09
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByMinuteFormat2(Calendar cal){
		return toString(cal, MINUTE_FORMAT2);
	}
	/**
	 * 将时间戳对象输出为yyyy-MM-dd 字符串 格式为 2011-09-01
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByDayFormat1(Timestamp timestamp){
		return toStringByDayFormat1(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy-MM-dd 字符串 格式为 2011-09-01
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByDayFormat1(Date date){
		return toStringByDayFormat1(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy-MM-dd 字符串 格式为 2011-09-01
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByDayFormat1(Calendar cal){
		return toString(cal, DAY_FORMAT1);
	}
	/**
	 * 将时间戳对象输出为yyyy/MM/dd字符串 格式为 2011/09/01
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByDayFormat2(Timestamp timestamp){
		return toStringByDayFormat2(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy/MM/dd字符串 格式为 2011/09/01
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByDayFormat2(Date date){
		return toStringByDayFormat2(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy/MM/dd字符串 格式为 2011/09/01
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByDayFormat2(Calendar cal){
		return toString(cal, DAY_FORMAT2);
	}
	
	/**
	 * 将时间戳对象输出为HH:mm:ss字符串 格式为 09:09:09
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat5(Timestamp timestamp){
		return toStringByFileFormat5(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为HH:mm:ss字符串 格式为 09:09:09
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat5(Date date){
		return toStringByFileFormat5(toCalendar(date));
	}
	/**
	 * 将日历对象输出为HH:mm:ss字符串 格式为 09:09:09
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat5(Calendar cal){
		return toString(cal, FILE_FORMAT5);
	}
	/**
	 * 将时间戳对象输出为HHmmss字符串 格式为 090909
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat4(Timestamp timestamp){
		return toStringByFileFormat4(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为HHmmss字符串 格式为 090909
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat4(Date date){
		return toStringByFileFormat4(toCalendar(date));
	}
	/**
	 * 将日历对象输出为HHmmss字符串 格式为 090909
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat4(Calendar cal){
		return toString(cal, FILE_FORMAT4);
	}

	
	/**
	 * 将时间戳对象输出为yyyyMMdd字符串 格式为 20110909
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat3(Timestamp timestamp){
		return toStringByFileFormat3(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyyMMdd字符串 格式为 20110909
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat3(Date date){
		return toStringByFileFormat3(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyyMMdd字符串 格式为 20110909
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat3(Calendar cal){
		return toString(cal, FILE_FORMAT3);
	}
	
	/**
	 * 将时间戳对象输出为yyyyMMddHHmmss字符串 格式为 20110909090909
	 * @param timestamp 时间戳
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat2(Timestamp timestamp){
		return toStringByFileFormat2(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyyMMddHHmmss字符串 格式为 20110909090909
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat2(Date date){
		return toStringByFileFormat2(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyyMMddHHmmss字符串 格式为 20110909090909
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat2(Calendar cal){
		return toString(cal, FILE_FORMAT2);
	}
	
	/**
	 * 将时间戳对象输出为yyyy-MM-dd_HH-mm-ss字符串 格式为 2011-09-09_09-09-09
	 * @param timestamp 时间戳
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat1(Timestamp timestamp){
		return toStringByFileFormat1(toCalendar(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy-MM-dd_HH-mm-ss字符串 格式为 2011-09-09_09-09-09
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat1(Date date){
		return toStringByFileFormat1(toCalendar(date));
	}
	
	/**
	 * 将日历对象输出为yyyy-MM-dd_HH-mm-ss字符串 格式为 2011-09-09_09-09-09
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toStringByFileFormat1(Calendar cal){
		return toString(cal, FILE_FORMAT1);
	}
	
	
	/**
	 * 将时间戳对象输出为yyyy年MM月dd日 HH时mm分ss秒 字符串 格式为 2011年09月09日 09时09分09秒
	 * @param timestamp 时间戳对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat1(Timestamp timestamp) {
		return toStringByChineseFormat1(toDate(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy年MM月dd日 HH时mm分ss秒 字符串 格式为 2011年09月09日 09时09分09秒
	 * @param date 日期对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat1(Date date) {
		return toStringByChineseFormat1(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy年MM月dd日 HH时mm分ss秒 字符串 格式为 2011年09月09日 09时09分09秒
	 * @param cal 日历对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat1(Calendar cal) {
		return toString(cal, CHINESE_FORMAT1);
	}
	/**
	 * 将时间戳对象输出为yyyy年MM月dd日 字符串 格式为 2011年09月09日
	 * @param timestamp 时间戳对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat2(Timestamp timestamp) {
		return toStringByChineseFormat2(toDate(timestamp));
	}
	/**
	 * 将日期对象输出为yyyy年MM月dd日 字符串 格式为 2011年09月09日
	 * @param date 日期对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat2(Date date) {
		return toStringByChineseFormat2(toCalendar(date));
	}
	/**
	 * 将日历对象输出为yyyy年MM月dd日 字符串 格式为 2011年09月09日
	 * @param cal 日历对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat2(Calendar cal) {
		return toString(cal, CHINESE_FORMAT2);
	}
	/**
	 * 将时间戳对象输出为HH时mm分ss秒 字符串 格式为 09时09分09秒
	 * @param timestamp 时间戳对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat3(Timestamp timestamp) {
		return toStringByChineseFormat3(toDate(timestamp));
	}
	/**
	 * 将日期对象输出为HH时mm分ss秒 字符串 格式为 09时09分09秒
	 * @param date 日期对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat3(Date date) {
		return toStringByChineseFormat3(toCalendar(date));
	}
	/**
	 * 将日历对象输出为HH时mm分ss秒 字符串 格式为 09时09分09秒
	 * @param cal 日历对象
	 * @return 日期字符串
	 */
	public static String toStringByChineseFormat3(Calendar cal) {
		return toString(cal, CHINESE_FORMAT3);
	}
	/**
	 * 将时间戳对象输出为字符串  格式为 201191 2011101 2011911 20111211
	 * @param timestamp 时间戳对象
	 * @return 字符串形式的日期
	 */
	public static String toString(Timestamp timestamp){
		return toString(toDate(timestamp));
	}
	
	/**
	 * 将日期对象输出为字符串  格式为 201191 2011101 2011911 20111211
	 * @param date 日期对象
	 * @return 字符串形式的日期
	 */
	public static String toString(Date date){
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return toString(gc);
	}
	/**
	 * 将日历对象输出为字符串 格式为 201191 2011101 2011911 20111211
	 * @param cal 日历对象
	 * @return 字符串形式的日期
	 */
	public static String toString(Calendar cal){
		String startDate = "";
		startDate = startDate + cal.get(Calendar.YEAR);
		int i = cal.get(Calendar.MONTH);
		i = i + 1;
		startDate = startDate + i;
		startDate = startDate + cal.get(Calendar.DATE);
		return startDate;
	}
	/**
	 * 校验结束日期到起始日期
	 * @param beginDate 起始日期
	 * @param endDate 结束日期
	 * @return 如果起始日期小于结束日期，返回为真，反之为假
	 * @throws Exception 异常
	 */
	public static boolean validate(String beginDate, String endDate) throws Exception {
		if (beginDate == null || endDate == null ) {
			return true;
		}
		Date beginDate1 = toDate(beginDate);
		Date endDate1 = toDate(endDate);
		return beginDate1.getTime() <= endDate1.getTime();
	}
}