package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Date;
import java.util.HashMap;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.Date2StringConverter;
import com.woate.talkingbird.framework.util.DateUtil;

public class Date2StringConverterTest {
	@Test
	public void testConvert1(){
		Converter c = new Date2StringConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Date2StringConverter.DATE_FORMAT, DateUtil.DAY_FORMAT1);
		String datestr = "2014-12-12";
		Date date = DateUtil.toDate(datestr);
		Object obj = c.convert(date, params);
		System.out.println(obj);
		Assert.assertEquals(datestr, obj);
	}
	@Test
	public void testConvert2(){
		Converter c = new Date2StringConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Date2StringConverter.DATE_FORMAT, DateUtil.DAY_FORMAT2);
		String datestr = "2014/12/12";
		Date date = DateUtil.toDate(datestr);
		Object obj = c.convert(date, params);
		System.out.println(obj);
		Assert.assertEquals(datestr, obj);
	}
	@Test
	public void testConvert3(){
		Converter c = new Date2StringConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Date2StringConverter.DATE_FORMAT, DateUtil.FILE_FORMAT2);
		String datestr = "20141212010101";
		Date date = DateUtil.toDate(datestr);
		Object obj = c.convert(date, params);
		System.out.println(obj);
		Assert.assertEquals(datestr, obj);
	}
	@Test
	public void testConvert4(){
		Converter c = new Date2StringConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Date2StringConverter.DATE_FORMAT, DateUtil.FILE_FORMAT3);
		String datestr = "20141212";
		Date date = DateUtil.toDate(datestr);
		Object obj = c.convert(date, params);
		System.out.println(obj);
		Assert.assertEquals(datestr, obj);
	}
	@Test
	public void testConvert5(){
		Converter c = new Date2StringConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Date2StringConverter.DATE_FORMAT, DateUtil.FILE_FORMAT4);
		String datestr = "010101";
		Date date = DateUtil.toDate(datestr);
		Object obj = c.convert(date, params);
		System.out.println(obj);
		Assert.assertEquals(datestr, obj);
	}
	@Test
	public void testConvert6(){
		Converter c = new Date2StringConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Date2StringConverter.DATE_FORMAT, DateUtil.FILE_FORMAT5);
		String datestr = "01:01:01";
		Date date = DateUtil.toDate(datestr);
		Object obj = c.convert(date, params);
		System.out.println(obj);
		Assert.assertEquals(datestr, obj);
	}
}
