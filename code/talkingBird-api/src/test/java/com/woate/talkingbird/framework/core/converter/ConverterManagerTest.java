package com.woate.talkingbird.framework.core.converter;

import java.math.BigDecimal;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.ConverterManager;

public class ConverterManagerTest {
	@Test
	public void testFind1(){
		Converter c1 = ConverterManager.find(String.class, byte[].class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.String2ByteArrayConverter/1.0", c1.getInfo());
		Converter c2 = ConverterManager.find(Date.class, String.class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.Date2StringConverter/1.0", c2.getInfo());
		Converter c21 = ConverterManager.find(Date.class, byte[].class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.Date2ByteArrayConverter/1.0", c21.getInfo());
		Converter c3 = ConverterManager.find(BigDecimal.class, byte[].class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.BigDecimal2ByteArrayConverter/1.0", c3.getInfo());
		Converter c4 = ConverterManager.find(Integer.class, byte[].class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.Integer2ByteArrayConverter/1.0", c4.getInfo());
		Converter c5 = ConverterManager.find(Integer.TYPE, byte[].class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.Integer2ByteArrayConverter/1.0", c5.getInfo());
		Converter c6 = ConverterManager.find(byte[].class, String.class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.ByteArray2StringConverter/1.0", c6.getInfo());
		Converter c7 = ConverterManager.find(byte[].class, Date.class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.ByteArray2DateConverter/1.0", c7.getInfo());
		Converter c8 = ConverterManager.find(byte[].class, BigDecimal.class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.ByteArray2BigDecimalConverter/1.0", c8.getInfo());
		Converter c9 = ConverterManager.find(byte[].class, Integer.class);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.ByteArray2IntegerConverter/1.0", c9.getInfo());
		Converter c10 = ConverterManager.find(byte[].class, Integer.TYPE);
		Assert.assertEquals("com.brilliance.framework.communication.converter.impl.ByteArray2IntegerConverter/1.0", c10.getInfo());
	}
}
