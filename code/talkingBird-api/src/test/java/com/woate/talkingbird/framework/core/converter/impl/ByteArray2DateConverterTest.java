package com.woate.talkingbird.framework.core.converter.impl;

import java.util.Date;
import java.util.HashMap;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2DateConverter;
import com.woate.talkingbird.framework.util.DateUtil;

public class ByteArray2DateConverterTest {
	@Test(expected=IllegalArgumentException.class)
	public void testNoInputEncodingConvert(){
		Converter c = new ByteArray2DateConverter();
		c.convert(new byte[10], new HashMap<String, Object>());
	}
	@Test(expected=NullPointerException.class)
	public void testNullParamsConvert(){
		Converter c = new ByteArray2DateConverter();
		c.convert(new byte[10], null);
	}
	@Test(expected=NullPointerException.class)
	public void testNullInConvert(){
		Converter c = new ByteArray2DateConverter();
		c.convert(null, new HashMap<String, Object>());
	}
	@Test
	public void testConvert1(){
		Converter c = new ByteArray2DateConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		Date obj = c.convert("20141212".getBytes(), params);
		System.out.println(obj);
		Assert.assertTrue(DateUtil.toString(obj).equals("20141212"));
	}
}
