package com.woate.talkingbird.framework.core.converter.impl;

import java.math.BigDecimal;
import java.util.HashMap;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.impl.ByteArray2BigDecimalConverter;

public class ByteArray2BigDecimalConverterTest {
	@Test(expected=NumberFormatException.class)
	public void testNoInputEncodingConvert(){
		Converter c = new ByteArray2BigDecimalConverter();
		c.convert(new byte[10], new HashMap<String, Object>());
	}
	@Test(expected=NullPointerException.class)
	public void testNullParamsConvert(){
		Converter c = new ByteArray2BigDecimalConverter();
		c.convert(new byte[10], null);
	}
	@Test(expected=NullPointerException.class)
	public void testNullInConvert(){
		Converter c = new ByteArray2BigDecimalConverter();
		c.convert(null, new HashMap<String, Object>());
	}
	@Test
	public void testConvert1(){
		Converter c = new ByteArray2BigDecimalConverter();
		HashMap<String, Object> params = new HashMap<String, Object>();
		Object obj = c.convert(new byte[]{(byte)0x31 , (byte)0x32 , (byte)0x33 , (byte)0x34 , (byte)0x35 , (byte)0x36}, params);
		System.out.println(obj);
		Assert.assertTrue(new BigDecimal("123456").compareTo((BigDecimal)obj) == 0);
	}
}
