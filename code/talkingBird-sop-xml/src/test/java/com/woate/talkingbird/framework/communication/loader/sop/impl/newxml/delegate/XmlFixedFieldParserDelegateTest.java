package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.net.URI;

import javax.xml.xpath.XPath;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSessionFactory;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlTester;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class XmlFixedFieldParserDelegateTest {
	@Test
	public void testParse1() throws Exception{
		XmlSessionFactory.createSession();
		URI uri = new File("test1/busi.xml").toURI();
		XPath xPath = XmlTester.initXpath(uri);
		XPathStack stack = new XPathStack();
		XmlSession.getSession().setAttribute(Constantes.PATH_STACK, stack) ;
		XmlSession.getSession().setAttribute(Constantes.XPATH, xPath);
		XmlSession.getSession().setAttribute(Constantes.URI, uri);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		AliasNameManager aliasNameManager = XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER);
		aliasNameManager.register("String", "java.lang.String");
		Node node = XmlTester.getNode(uri, "interfaces/response/fixed-field");
		stack.push("interfaces/response/fixed-field");
		XmlFixedFieldParserDelegate parser = new XmlFixedFieldParserDelegate();
		XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, node);
		parser.parse();
		SOPFixedFieldImpl field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
		Assert.assertEquals("BUSI_FIELD3", field.getName());
		Assert.assertEquals("BUSI_FIELD3_RSP", field.getAlias());
		Assert.assertEquals("����ģʽ", field.getDesc());
		Assert.assertEquals(1, field.getMaxLength());
		Assert.assertEquals(String.class, field.getClazz());
		System.out.println(field);
	}
	
	

}
