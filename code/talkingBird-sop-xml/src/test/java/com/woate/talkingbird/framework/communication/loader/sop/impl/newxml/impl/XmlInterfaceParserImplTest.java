package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl;

import java.io.File;
import java.net.URI;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class XmlInterfaceParserImplTest {
	@Test
	public void testParse1(){
		XmlSession.destory();
		AliasNameManager aliasNameManager = AliasNameManager.getSimpleManager();
		XmlSession.getSession().setAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER, aliasNameManager);
		URI uri = new File( "test1/Test_interface.xml").toURI();
		XmlInterfaceParserImpl parser = new XmlInterfaceParserImpl();
		MainMetadataLoader<SOPMetadata> mainLoader = parser.parse(uri);
		System.out.println(mainLoader);
	}
}
