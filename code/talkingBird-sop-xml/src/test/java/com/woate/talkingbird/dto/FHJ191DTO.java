package com.woate.talkingbird.dto;

import java.math.BigDecimal;

public class FHJ191DTO {
	String prodId;
	BigDecimal floatProfitAndLoss;
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public BigDecimal getFloatProfitAndLoss() {
		return floatProfitAndLoss;
	}
	public void setFloatProfitAndLoss(BigDecimal floatProfitAndLoss) {
		this.floatProfitAndLoss = floatProfitAndLoss;
	}
	@Override
	public String toString() {
		return "FHJ191DTO [prodId=" + prodId + ", floatProfitAndLoss=" + floatProfitAndLoss + "]";
	}
	
}
