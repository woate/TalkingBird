package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.net.URI;

import javax.xml.xpath.XPath;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSessionFactory;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlTester;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class XmlObjectParserDelegateTest {
	@Test
	public void testParse1() throws Exception{
		XmlSessionFactory.createSession();
		URI uri = new File("test1/Test_interface.xml").toURI();
		XPath xPath = XmlTester.initXpath(uri);
		Document document = XmlTester.initDoc(uri);
		XPathStack stack = new XPathStack();
		XmlSession.getSession().setAttribute(Constantes.PATH_STACK, new XPathStack()) ;
		XmlSession.getSession().setAttribute(Constantes.DOCUMENT, document);
		XmlSession.getSession().setAttribute(Constantes.XPATH, xPath);
		XmlSession.getSession().setAttribute(Constantes.URI, uri);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		AliasNameManager aliasNameManager = XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER);
		aliasNameManager.register("Integer", "java.lang.Integer");
		aliasNameManager.register("String", "java.lang.String");
		aliasNameManager.register("OHJ190DTO", "com.woate.talkingbird.dto.OHJ190DTO");
		XmlMetadatasPoolParserDelegate parser1 = new XmlMetadatasPoolParserDelegate();
		parser1.parse();
		Node node = XmlTester.getNode(uri, "interfaces/interface/request/busis/busi/object");
		stack.push("interfaces/interface/request/busis/busi/object");
		XmlObjectParserDelegate parser = new XmlObjectParserDelegate();
		XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, node);
		parser.parse();
		SOPMetadata field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
		System.out.println(field);
	}
}
