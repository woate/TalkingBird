package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi;

import java.nio.ByteBuffer;

import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.sop.SOPMacGenCallback;
import com.woate.talkingbird.framework.communication.sop.SOPPacket;
import com.woate.talkingbird.framework.communication.sop.SOPPacketSFacade;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.communication.sop.impl.SOPMacVlidateCallbackImpl;

/**
 * 测试接口test3/test_interface.xml
 * @author liucheng
 *
 */
public class Test3 {
	/**
	 * 测试不定长字段的必填属性desc,min-len,max-len,class,allow-null
	 */
	@Test
	public void test1(){
		try {
			SOPSession.destory();
			XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("test3");
			loader.load();
			SOPPacket packet = new SOPPacketSFacade("Test", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVlidateCallbackImpl());
			ByteBuffer byteBuffer = packet.format();
			Assert.fail("不该运行到此处");
		} catch (Exception e) {
			//e.printStackTrace();
			String msg = e.getMessage();
		}
	}
}
