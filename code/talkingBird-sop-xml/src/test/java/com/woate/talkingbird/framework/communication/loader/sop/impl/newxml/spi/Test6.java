package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.woate.talkingbird.dto.ERR000DTO;
import com.woate.talkingbird.dto.FHJ191DTO;
import com.woate.talkingbird.dto.OHJ190DTO;
import com.woate.talkingbird.dto.OHJ191DTO;
import com.woate.talkingbird.framework.communication.Dictionary;
import com.woate.talkingbird.framework.communication.TransformException;
import com.woate.talkingbird.framework.communication.sop.SOPBusiWrapper;
import com.woate.talkingbird.framework.communication.sop.SOPMacGenCallback;
import com.woate.talkingbird.framework.communication.sop.SOPMacVildateCallback;
import com.woate.talkingbird.framework.communication.sop.SOPPacket;
import com.woate.talkingbird.framework.communication.sop.SOPPacketRFacade;
import com.woate.talkingbird.framework.communication.sop.SOPPacketSFacade;
import com.woate.talkingbird.framework.communication.sop.SOPSession;
import com.woate.talkingbird.framework.util.Hex;

public class Test6 {
	@Test
	public void test1() throws Exception{
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("interfaces");
		loader.load();
		SOPPacket packet = new SOPPacketSFacade("HJ19", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
			SOPSession.destory();
			SOPSession session = SOPSession.getSession();
			session.setAttribute(Dictionary.YNGYJG, "1001");
			session.setAttribute(Dictionary.JIO1GY, "901328");
			SOPBusiWrapper busiWrapper = session.createBusiData();
			OHJ190DTO ohj190dto = new OHJ190DTO();
			ohj190dto.setBankNo("62XXXXXXXXX1234");
			ohj190dto.setProdId("Au99.99");
			busiWrapper.setBusidata("OHJ190", ohj190dto);
			ByteBuffer buffer = packet.format();
			System.out.println(Hex.toDisplayString(buffer.array()));
//			Hex.write(buffer.array(), new FileOutputStream("test1_1.hex"));
			Hex.write(buffer.array(), new FileOutputStream("test1_hj19.hex"));
	}
	@Test
	public void test2() throws Exception{
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("interfaces");
		loader.load();
		SOPPacket packet = new SOPPacketRFacade("HJ19", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
		SOPSession.destory();
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.YNGYJG, "1001");
		session.setAttribute(Dictionary.JIO1GY, "901328");
		SOPBusiWrapper busiWrapper = session.createBusiData();
		OHJ191DTO ohj191dto = new OHJ191DTO();
		ohj191dto.setTotalAmount(123);
		busiWrapper.setBusidata("OHJ191", ohj191dto);
		FHJ191DTO fhj191dto1 = new FHJ191DTO();
		fhj191dto1.setFloatProfitAndLoss(BigDecimal.TEN);
		fhj191dto1.setProdId("Au99.99");
		FHJ191DTO fhj191dto2 = new FHJ191DTO();
		fhj191dto2.setFloatProfitAndLoss(BigDecimal.ONE);
		fhj191dto2.setProdId("Au99.95");
		FHJ191DTO fhj191dto3 = new FHJ191DTO();
		fhj191dto3.setFloatProfitAndLoss(BigDecimal.ONE);
		fhj191dto3.setProdId("Au99.95");
		List<FHJ191DTO> list = new ArrayList<FHJ191DTO>();
		list.add(fhj191dto1);
		list.add(fhj191dto2);
		list.add(fhj191dto3);
		busiWrapper.setBusidata("FHJ191", list);
		ByteBuffer buffer = packet.format();
//		System.out.println(Hex.toDisplayString(buffer.array()));
		Hex.write(buffer.array(), new FileOutputStream("test2_hj19.hex"));
	}
	@Test
	public void test2err() throws Exception{
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("interfaces");
		loader.load();
		SOPPacket packet = new SOPPacketRFacade("HJ19", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
		SOPSession.destory();
		SOPSession session = SOPSession.getSession();
		session.setAttribute(Dictionary.PTCWDH, "GT00111");
		session.setAttribute(Dictionary.YNGYJG, "1001");
		session.setAttribute(Dictionary.JIO1GY, "901328");
		SOPBusiWrapper busiWrapper = session.createBusiData();
		ERR000DTO err000dto = new ERR000DTO();
		err000dto.setErrorCode("GT00111");
		err000dto.setErrorMsg("111111");
		busiWrapper.setBusidata("ERR000", err000dto);
		ByteBuffer buffer = packet.format();
		System.out.println(Hex.toDisplayString(buffer.array()));
		Hex.write(buffer.array(), new FileOutputStream("test2_hj19.hex"));
	}
	
	@Test
	public void test3() throws TransformException, FileNotFoundException{
		SOPSession.destory();
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("interfaces");
		loader.load();
		SOPPacket packet = new SOPPacketRFacade("HJ19", loader,  new SOPMacGenCallback() {
			@Override
			public void call() {
			}
		}, new SOPMacVildateCallback() {
			@Override
			public void vildate() {
			}
		});
		byte[] array = Hex.read(new FileInputStream("test1_hj19.hex"));
		Hex.write(array);
		ByteBuffer buffer = ByteBuffer.wrap(array);
		buffer.rewind();
		packet.parse(buffer);
		SOPSession session = SOPSession.getSession();
		System.out.println(session.attributes());
		System.out.println(session.peekFirstBusiData().attributes());
		System.out.println(session.pollFirstBusiData().busidatas());
	}
	@Test
	public void test4() throws TransformException, FileNotFoundException{
		XmlBootstrapByScanLoader loader = new XmlBootstrapByScanLoader("interfaces");
		loader.load();
			SOPSession.destory();
			SOPPacket packet = new SOPPacketSFacade("HJ19", loader,  new SOPMacGenCallback() {
				@Override
				public void call() {
				}
			}, new SOPMacVildateCallback() {
				@Override
				public void vildate() {
				}
			});
			byte[] array = Hex.read(new FileInputStream("test2_hj19.hex"));
			Hex.write(array);
			ByteBuffer buffer = ByteBuffer.wrap(array);
			buffer.rewind();
			packet.parse(buffer);
			SOPSession session = SOPSession.getSession();
			System.out.println(session.attributes());
			System.out.println(session.peekFirstBusiData().attributes());
			System.out.println(session.pollFirstBusiData().busidatas());
	}
}
