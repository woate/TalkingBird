package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSessionFactory;
import com.woate.talkingbird.framework.core.AliasNameManager;

public class XmlMetadatasPoolParserDelegateTest {
	/**
	 * 测试解析元信息池中的定长字段是否成功解析
	 * @throws Exception
	 */
	@Test
	public void testParse1() throws Exception{
		XmlSessionFactory.createSession();
		URI uri = new File("test1/busi.xml").toURI();
		Document document = initDoc(uri);
		XPath xPath = initXpath(uri);
		XmlSession.getSession().setAttribute(Constantes.PATH_STACK, new XPathStack()) ;
		XmlSession.getSession().setAttribute(Constantes.DOCUMENT, document);
		XmlSession.getSession().setAttribute(Constantes.XPATH, xPath);
		XmlSession.getSession().setAttribute(Constantes.URI, uri);
		XmlSession.getSession().setAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER, AliasNameManager.getSimpleManager());
		AliasNameManager aliasNameManager = XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER);
		aliasNameManager.register("Integer", "java.lang.Integer");
		aliasNameManager.register("String", "java.lang.String");
		XmlMetadatasPoolParserDelegate parser = new XmlMetadatasPoolParserDelegate();
		parser.parse();
		System.out.println(XmlSession.getSession().getAttribute(Constantes.METADATAS_POOL));
	}
	
	Document initDoc(URI uri) throws ParserConfigurationException, MalformedURLException, SAXException, IOException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(false);
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(uri.toURL().openStream());
		return document;
	}
	
	XPath initXpath(URI uri){
		XPathFactory xfactory = XPathFactory.newInstance();
		XPath xpath = xfactory.newXPath();
		return xpath;
	}
}
