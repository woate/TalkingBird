package com.woate.talkingbird.framework.communication.sop;


import junit.framework.Assert;

import org.junit.Test;

import com.woate.talkingbird.framework.communication.TransformException;

public class SOPFactoryTest {
	@Test
	public void testRStyle() throws TransformException{
		SOPFactory factory = SOPFactory.getDefaultFactory().load();
		SOPPacket packet = factory.getSOPPacketR("HJ19");
		Assert.assertNotNull(packet);
	}
	@Test
	public void testSStyle() throws TransformException{
		SOPFactory factory = SOPFactory.getDefaultFactory().load();
		SOPPacket packet = factory.getSOPPacketS("HJ19");
		Assert.assertNotNull(packet);
	}
}
