/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.sop.impl;

import com.woate.talkingbird.framework.communication.loader.BootstrapMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.spi.XmlBootstrapByScanLoader;
import com.woate.talkingbird.framework.communication.sop.SOPFactory;
import com.woate.talkingbird.framework.communication.sop.SOPPacket;
import com.woate.talkingbird.framework.communication.sop.SOPPacketRFacade;
import com.woate.talkingbird.framework.communication.sop.SOPPacketSFacade;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;


/**
 * SOP报文工厂类
 * @author liucheng
 * 2014-11-24 下午01:42:30
 *
 */
public class DefaultSOPFactoryImpl extends SOPFactory{
	static BootstrapMetadataLoader<SOPMetadata> bootstrapLoader;

	public SOPPacket getSOPPacketR(String name){
		MainMetadataLoader<SOPMetadata> mainLoader = getMainLoader(name);
		return new SOPPacketRFacade(name, mainLoader);
	}
	public SOPPacket getSOPPacketS(String name){
		MainMetadataLoader<SOPMetadata> mainLoader = getMainLoader(name);
		return new SOPPacketSFacade(name, mainLoader);
	}
	private MainMetadataLoader<SOPMetadata> getMainLoader(String name) {
		MainMetadataLoader<SOPMetadata> mainLoader = bootstrapLoader.getLoader(name);
		if(mainLoader == null){
			throw new NullPointerException("获取["+ name +"]交易的主加载器失败,可能没有扫描到!");
		}
		return mainLoader;
	}
	
	public SOPFactory load(){	
		bootstrapLoader = new XmlBootstrapByScanLoader("interfaces").load();
		return this;
	}
}
