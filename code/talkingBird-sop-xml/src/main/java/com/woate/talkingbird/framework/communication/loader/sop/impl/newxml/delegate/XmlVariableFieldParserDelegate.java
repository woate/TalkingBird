/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import static com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlNodeVildateUtil.*;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.FindClassByAliasUtil;
import com.woate.talkingbird.framework.communication.metadata.ValueMode;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldBuilder;
import com.woate.talkingbird.framework.core.converter.Converter;
import com.woate.talkingbird.framework.core.converter.ConverterManager;
/**
 * 解析可变字段解析器代理类
 * @author liucheng
 *
 */
public class XmlVariableFieldParserDelegate implements XmlParser{
	@Override
	public void parse() throws XmlParseException {
		try {
			XmlSession.getSession().setAttribute(Constantes.CURRENT_OBJECT,parse0());
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException(e);
		}
	}
	
	SOPMetadata parse0() throws Exception{
		Node node = XmlSession.getSession().getAttribute(Constantes.CURRENT_NODE);
		NamedNodeMap map = node.getAttributes();
		SOPVariableField field = null;
		Node nameNode = map.getNamedItem(Constantes.VARIABLE_FIELD_NAME);
		Node aliasNode = map.getNamedItem(Constantes.VARIABLE_FIELD_ALIAS);
		Node descNode = map.getNamedItem(Constantes.VARIABLE_FIELD_DESC);
		Node maxlenNode = map.getNamedItem(Constantes.VARIABLE_FIELD_MAX_LEN);
		Node minlenNode = map.getNamedItem(Constantes.VARIABLE_FIELD_MIN_LEN);
		Node allowNullNode = map.getNamedItem(Constantes.VARIABLE_FIELD_ALLOW_NULL);
		Node encodingNode = map.getNamedItem(Constantes.VARIABLE_FIELD_ENCODING);
		Node clazzNode = map.getNamedItem(Constantes.VARIABLE_FIELD_CLASS);
		Node valueNode = map.getNamedItem(Constantes.VARIABLE_FIELD_VALUE);
		Node valueModeNode = map.getNamedItem(Constantes.VARIABLE_FIELD_VALUE_MODE);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "name");
		String name = getValueBySafety(nameNode);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "alias");
		String alias = getValueBySafety(aliasNode);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "desc");
		String desc = getValueBySafety(descNode);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "max-len");
		int maxlen = string2number(getValueBySafety(maxlenNode));
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "min-len");
		int minlen = string2number(getValueBySafety(minlenNode));
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "encoding");
		String encoding = getValueBySafety(encodingNode);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "class");
		String clazz = getValueBySafety(clazzNode);
		// 根据别名获取类
		Class<?> clazz1 = FindClassByAliasUtil.findByAlias(clazz);
		// 编排转换器
		Converter formatConvert = ConverterManager.find(clazz1, byte[].class);
		Converter parseConvert = ConverterManager.find(byte[].class, clazz1);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "allow-null");
		boolean allowNull = string2boolean(getValueBySafety(allowNullNode), false);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "value");
		String value = getValueBySafety(valueNode);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "value-mode");
		String valueMode = getValueBySafety(valueModeNode);
		//如果不存在取值模式则默认设置为用户模式
		if(valueMode.isEmpty()){
			valueMode= ValueMode.USER.toString();
		}
		field = new SOPVariableFieldBuilder(name)
					.setAlias(alias)
					.setDesc(desc)
					.setEncoding(encoding)
					.setMinLength(minlen)
					.setMaxLength(maxlen)
					.setClazz(clazz1)
					.setFormatConverter(formatConvert)
					.setParseConverter(parseConvert)
					.setValue(value)
					.setValueMode(ValueMode.valueOf(valueMode.toUpperCase()))
					.setAllowNull(allowNull)
					.build();
		return field;
	}

}
