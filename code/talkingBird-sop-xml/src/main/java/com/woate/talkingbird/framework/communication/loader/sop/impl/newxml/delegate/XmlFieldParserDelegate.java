/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import static com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util.XmlNodeVildateUtil.*;

import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPFixedField;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPVariableField;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPFixedFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldBuilder;
import com.woate.talkingbird.framework.communication.sop.metadata.impl.SOPVariableFieldImpl;
import com.woate.talkingbird.framework.communication.sop.metadata.writeable.SOPVariableFieldWriteable;
/**
 * 解析引用型字段的解析代理类
 * @author liucheng
 *
 */
public class XmlFieldParserDelegate implements XmlParser{
	@Override
	public void parse() throws XmlParseException {
		try {
			XmlSession.getSession().setAttribute(Constantes.CURRENT_OBJECT,parse0());
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException(e);
		}
	}
	
	SOPMetadata parse0() throws Exception{
		Node node = XmlSession.getSession().getAttribute(Constantes.CURRENT_NODE);
		NamedNodeMap map = node.getAttributes();
		Node refNode = map.getNamedItem(Constantes.FIELD_REF);
		Node aliasNode = map.getNamedItem(Constantes.FIELD_ALIAS);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "ref");
		String ref = getValueBySafety(refNode);
		XmlSession.getSession().setAttribute(Constantes.PARSING_NAME, "alias");
		String alias = getValueBySafety(aliasNode);
		Map<String, SOPMetadata> metadataPool = XmlSession.getSession().getAttribute(Constantes.METADATAS_POOL);
		SOPVariableFieldWriteable newField = null; 
		SOPMetadata refField =  metadataPool.get(ref);
		if(refField == null){
			throw new XmlParseException("元信息池中未定义名为["+ ref +"]的元信息!");
		}
		if(refField instanceof SOPVariableFieldImpl){
			newField = new SOPVariableFieldBuilder(refField.getName()).clone((SOPVariableField)refField);
		}else if(refField instanceof SOPFixedFieldImpl){
			newField = new SOPFixedFieldBuilder(refField.getName()).clone((SOPFixedField)refField);
		}
		newField.setAlias(alias);
		return newField;
	}

}
