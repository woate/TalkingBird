/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.woate.talkingbird.framework.communication.loader.BootstrapMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.BusiDataMetadateLoader;
import com.woate.talkingbird.framework.communication.loader.MainMetadataLoader;
import com.woate.talkingbird.framework.communication.loader.MetadataLoader;
import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
import com.woate.talkingbird.framework.core.AliasNameManager;

/**
 * 基于newXml格式的主加载器实现
 * 
 * @author liucheng
 * 
 */
public class XmlMainMetadataLoader implements MainMetadataLoader<SOPMetadata> {
	final String name;
	boolean binit = false;
	/**
	 * 该别名管理器管理别名映射定义的别名
	 */
	AliasNameManager aliasNameManager = null;
	/**
	 * 该别名管理器管理公共信息头中定义的别名
	 */
	AliasNameManager comAliasNameManager = AliasNameManager.getSimpleManager();
	/**
	 * 该别名管理器管理系统信息头中定义的别名
	 */
	AliasNameManager sysAliasNameManager = AliasNameManager.getSimpleManager();
	final BootstrapMetadataLoader<SOPMetadata> bootstrapLoader;
	final URI uri;
	final Map<PacketType, List<SOPMetadata>> sysheaders = new HashMap<PacketType, List<SOPMetadata>>();
	final Map<PacketType, List<SOPMetadata>> comheaders = new HashMap<PacketType, List<SOPMetadata>>();
	final Map<String, XmlBusiDataMetadataLoader> busiLoades = new HashMap<String, XmlBusiDataMetadataLoader>();
	/**
	 * 系统信息头文件名
	 */
	String sysHeadFileName = null;
	/**
	 * 公共信息头文件名
	 */
	String comHeadFileName = null;
	final List<String> busis = new ArrayList<String>();

	final MetadataLoader<SOPMetadata> sysLoader = new MetadataLoader<SOPMetadata>() {
		
		@Override
		public MetadataLoader<SOPMetadata> load() {
			return this;
		}

		@Override
		public List<SOPMetadata> listHeader(PacketType type) {
			return sysheaders.get(type);
		}

		@Override
		public AliasNameManager getAliasNameManager() {
			return sysAliasNameManager;
		}

		@Override
		public MainMetadataLoader<SOPMetadata> getMainLoader() {
			return XmlMainMetadataLoader.this;
		}

		@Override
		public String toString() {
			return "[sysAliasNameManager=" + sysAliasNameManager + ", sysheaders=" + sysheaders + "]";
		}

		@Override
		public String getHeadFileName() {
			return sysHeadFileName;
		}
		
	};

	final MetadataLoader<SOPMetadata> comLoader = new MetadataLoader<SOPMetadata>() {
		@Override
		public MetadataLoader<SOPMetadata> load() {
			return this;
		}

		@Override
		public List<SOPMetadata> listHeader(PacketType type) {
			return comheaders.get(type);
		}

		@Override
		public AliasNameManager getAliasNameManager() {
			return comAliasNameManager;
		}

		@Override
		public MainMetadataLoader<SOPMetadata> getMainLoader() {
			return XmlMainMetadataLoader.this;
		}

		@Override
		public String toString() {
			return "[comAliasNameManager=" + comAliasNameManager + ", comheaders=" + comheaders + "]";
		}

		@Override
		public String getHeadFileName() {
			return comHeadFileName;
		}
		
	};

	public XmlMainMetadataLoader(String name, URI uri, BootstrapMetadataLoader<SOPMetadata> bootstrapLoader) {
		super();
		this.name = name;
		this.uri = uri;
		this.bootstrapLoader = bootstrapLoader;
		sysheaders.put(PacketType.REQ, new ArrayList<SOPMetadata>());
		sysheaders.put(PacketType.RSP, new ArrayList<SOPMetadata>());
		sysheaders.put(PacketType.EXP, new ArrayList<SOPMetadata>());
		comheaders.put(PacketType.REQ, new ArrayList<SOPMetadata>());
		comheaders.put(PacketType.RSP, new ArrayList<SOPMetadata>());
		comheaders.put(PacketType.EXP, new ArrayList<SOPMetadata>());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public MainMetadataLoader<SOPMetadata> load() {
		this.binit = false;
		//加载系统信息头
		sysLoader.load();
		//加载公共信息头
		comLoader.load();
		//加载交易加载器
		for (BusiDataMetadateLoader<SOPMetadata> loader : busiLoades.values()) {
			loader.load();
		}
		this.binit = true;
		return this;
	}

	@Override
	public MetadataLoader<SOPMetadata> getSysHeaderLoader() {
		return sysLoader;
	}

	@Override
	public MetadataLoader<SOPMetadata> getComHeaderLoader() {
		return comLoader;
	}

	@Override
	public BusiDataMetadateLoader<SOPMetadata>[] getBusiDataLoaders() {
		if (!binit) {
			throw new XmlParseException("XmlBusiDataMetadataLoader is not loaded!");
		}
		BusiDataMetadateLoader<SOPMetadata>[] loaders = new BusiDataMetadateLoader[busis.size()];
		for (int i = 0; i < busis.size(); i++) {
			String tradeCode = busis.get(i);
			loaders[i] = busiLoades.get(tradeCode);
		}
		return loaders;
	}

	@Override
	public AliasNameManager getAliasNameManager() {
		return aliasNameManager;
	}

	@Override
	public BootstrapMetadataLoader<SOPMetadata> getBoostrapLoader() {
		return bootstrapLoader;
	}

	/**
	 * 获取新的或者已经存在的加载器
	 * 
	 * @param tradeCode
	 *            交易码
	 * @return 交易元数据加载器
	 */
	public XmlBusiDataMetadataLoader getNewOrExist(String tradeCode) {
		if (busiLoades.containsKey(tradeCode)) {
			return (XmlBusiDataMetadataLoader) busiLoades.get(tradeCode);
		} else {
			XmlBusiDataMetadataLoader newloader = new XmlBusiDataMetadataLoader(tradeCode, this);
			busiLoades.put(tradeCode, newloader);
			busis.add(tradeCode);
			return newloader;
		}
	}


	@Override
	public String toString() {
		return "XmlMainMetadataLoader [name=" + name + ", aliasNameManager=" + aliasNameManager + ", sysLoader=" + sysLoader + ", comLoader=" + comLoader + ", busiLoades=" + busiLoades + "]";
	}

	public void setAliasNameManager(AliasNameManager aliasNameManager) {
		this.aliasNameManager = aliasNameManager;
	}
	
	

}
