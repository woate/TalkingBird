/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml;

import java.net.URI;
import java.util.List;

import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
/**
 * Xml头信息解析器接口<br>
 * @author liucheng
 *
 */
public interface XmlHeaderParser {
	/**
	 * 解析头信息文件
	 * @throws XmlParseException
	 * @return
	 */
	public List<SOPMetadata> parse(URI uri)throws XmlParseException;

}
