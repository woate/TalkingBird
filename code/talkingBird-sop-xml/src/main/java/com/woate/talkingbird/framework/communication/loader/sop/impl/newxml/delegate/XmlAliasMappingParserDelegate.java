/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.net.URI;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.core.AliasNameException;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 别名映射解析代理类
 * @author liucheng
 *
 */
public class XmlAliasMappingParserDelegate implements XmlParser{

	@Override
	public void parse() throws XmlParseException {
		try {
			parseAliasMapping();
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException(e);
		}
	}
	/**
	 * 解析别名映射节点
	 * @throws XmlParseException
	 */
	void parseAliasMapping() throws Exception {
		XPathStack pathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		URI uri = XmlSession.getSession().getAttribute(Constantes.URI);
		Document document = XmlSession.getSession().getAttribute(Constantes.DOCUMENT);
		XPath xpath = XmlSession.getSession().getAttribute(Constantes.XPATH);
		pathStack.push(Constantes.INTERFACES_ALIAS_MAPPING);
		XPathExpression aliasMappingExpr = xpath.compile(Constantes.INTERFACES_ALIAS_MAPPING);
		Node mappingNode = null;
		try {
			mappingNode = (Node) aliasMappingExpr.evaluate(document, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmlParseException("载入[" + uri + "]别名映射失败", e);
		}
		if (mappingNode == null) {
			throw new XmlParseException("/interfaces/alias-mapping 节点不存在");
		}
		NodeList aliasNodeSet = mappingNode.getChildNodes();
		parseMapping(aliasNodeSet);
		pathStack.poll();
	}
	/**
	 * 解析别名映射节点
	 * @param nodeSet
	 * @param aliasMap
	 */
	void parseMapping(NodeList nodeSet){
		for (int i = 0; i < nodeSet.getLength(); i++) {
			Node node = nodeSet.item(i);
			if(Constantes.MAPPING.equals(node.getNodeName())){
				NamedNodeMap namedNodeMap = node.getAttributes();
				parseAlias(namedNodeMap);				
			}
		}
	}
	/**
	 * 解析别名属性
	 * @param namedNodeMap
	 * @return
	 */
	void parseAlias(NamedNodeMap namedNodeMap){
		Node alias = namedNodeMap.getNamedItem(Constantes.ALIAS);
		Node real = namedNodeMap.getNamedItem(Constantes.REAL);
		Node domain = namedNodeMap.getNamedItem(Constantes.DOMAIN);
		AliasNameManager aliasNameManager = null;
		//否则是局部的，使用局部别名管理器注册
		if(domain==null || Constantes.INNER.equals(domain.getNodeValue())){
			aliasNameManager = XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER);
			String realName = aliasNameManager.findRealName(alias.getNodeValue());
			if(realName != null && !realName.equals(real.getNodeValue())){
				throw new AliasNameException("局部别名管理器中已经注册过别名["+alias.getNodeValue() +"],已注册映射到["+ real +"],将注册映射到["+ real.getNodeValue()+"],因此发生冲突!");
			}
			if(realName != null && realName.equals(real.getNodeValue())){
				System.err.print("局部别名管理器中已经注册过别名["+alias.getNodeValue() +"],已注册映射到["+ real +"],发生重复注册,跳过!\n");
			}
			if(realName == null){				
				aliasNameManager.register(alias.getNodeValue(), real.getNodeValue());
			}
			//如果是全局的，则使用全局别名管理器注册
		}else{
			aliasNameManager = XmlSession.getSession().getAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER);
			String realName = aliasNameManager.findRealName(alias.getNodeValue());
			if(realName != null && !realName.equals(real.getNodeValue())){
				throw new AliasNameException("全局别名管理器中已经注册过别名["+alias.getNodeValue() +"],已注册映射到["+ real +"],将注册映射到["+ real.getNodeValue()+"],因此发生冲突!");
			}
			if(realName != null && realName.equals(real.getNodeValue())){
				System.err.print("全局别名管理器中已经注册过别名["+alias.getNodeValue() +"],已注册映射到["+ real +"],发生重复注册,跳过!\n");
			}
			if(realName == null){				
				aliasNameManager.register(alias.getNodeValue(), real.getNodeValue());
			}
		}
	}
}
