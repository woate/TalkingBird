/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlDocumentParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
/**
 * 抽象的Xml解析器实现
 * @author liucheng
 *
 */
public abstract class AbstractXmlParserImpl implements XmlDocumentParser{
	
	@Override
	public void vildateSchema(URI uri,String xsdPath) throws XmlParseException {
		try {
			SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			URL xsdURL = Thread.currentThread().getContextClassLoader().getResource(xsdPath);
			Schema schema = schemaFactory.newSchema(xsdURL);
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(uri.toASCIIString()));
		} catch (Exception e) {
			throw new XmlParseException("解析接口文件["+ uri+"]发生异常!",e);
		}
	}

	@Override
	public Document initDoc(URI uri) throws XmlParseException {
		InputStream is = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(false);
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			is = uri.toURL().openStream();
			Document document = builder.parse(is);
			return document;
		} catch (Exception e) {
			throw new XmlParseException("加载接口文件["+ uri+"]发生异常!",e);
		}finally{
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					throw new XmlParseException(e);
				}
			}
		}
	}

	@Override
	public XPath initXpath(URI uri) throws XmlParseException {
		XPathFactory xfactory = XPathFactory.newInstance();
		XPath xpath = xfactory.newXPath();
		return xpath;
	}

}
