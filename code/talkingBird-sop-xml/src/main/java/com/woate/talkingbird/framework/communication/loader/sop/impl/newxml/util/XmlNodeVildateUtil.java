/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util;

import java.net.URI;

import org.w3c.dom.Node;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XPathStack;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
/**
 * Xml节点验证工具
 * @author liucheng
 *
 */
public class XmlNodeVildateUtil {

	/**
	 * 将字符串转换薇布尔值
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean string2boolean(String obj) {
		String name = XmlSession.getSession().getAttribute(Constantes.PARSING_NAME);
		URI uri = XmlSession.getSession().getAttribute(Constantes.URI);
		XPathStack pathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		String value = stringNotEmpty(obj);
		if (!value.equalsIgnoreCase("true") && !value.equalsIgnoreCase("false")) {
			throw new XmlParseException("文件[" + uri + "]路径[" + pathStack + "]属性[" + name + "] 不为布尔值，只能设置true/false!");
		}
		boolean b = Boolean.valueOf(value);
		return b;
	}
	/**
	 * 将字符串转换为布尔值，如果为空则返回默认值
	 * @param obj 
	 * @param def 默认值
	 * @return
	 */
	public static boolean string2boolean(String obj, boolean def) {
		if(obj == null || obj.trim().isEmpty()){
			return def;
		}
		if(obj.trim().equalsIgnoreCase("true")){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 确定节点是否为空
	 * 
	 * @param obj
	 * @return
	 */
	public static String stringNotEmpty(String obj) {
		String name = XmlSession.getSession().getAttribute(Constantes.PARSING_NAME);
		URI uri = XmlSession.getSession().getAttribute(Constantes.URI);
		XPathStack pathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		if (obj.isEmpty()) {
			throw new XmlParseException("文件[" + uri + "]路径[" + pathStack + "]属性[" + name + "] 为空!");
		}
		return obj;
	}
	/**
	 * 确定节点是否为空
	 * @param obj
	 * @param def 默认值
	 * @return
	 */
	public static String stringNotEmpty(String obj, String def) {
		if (obj.isEmpty()) {
			return def;
		}
		return obj;
	}
	/**
	 * 将字符串转换为整型
	 * @param obj
	 * @return
	 */
	public static int string2number(String obj) {
		String name = XmlSession.getSession().getAttribute(Constantes.PARSING_NAME);
		URI uri = XmlSession.getSession().getAttribute(Constantes.URI);
		XPathStack pathStack = XmlSession.getSession().getAttribute(Constantes.PATH_STACK);
		int i = 0;
		try {
			i = Integer.valueOf(obj);
		} catch (Exception e) {
			throw new XmlParseException("文件[" + uri + "]路径[" + pathStack + "]属性[" + name + "] 不是数字!", e);
		}
		return i;
	}

	/**
	 * 安全的获得节点值
	 * 
	 * @param node
	 * @return
	 */
	public static String getValueBySafety(Node node) {
		if (node == null) {
			return "";
		} else {
			return node.getNodeValue();
		}
	}
}
