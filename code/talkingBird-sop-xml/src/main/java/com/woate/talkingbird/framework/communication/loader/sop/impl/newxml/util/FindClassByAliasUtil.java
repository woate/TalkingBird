/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.util;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.core.AliasNameManager;
/**
 * 按照别名查找类工具
 * @author liucheng
 *
 */
public class FindClassByAliasUtil {
	/**
	 * 根据别名查找类
	 * 
	 * @param alias
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static Class<?> findByAlias(String alias) throws ClassNotFoundException {
		AliasNameManager currentAliasNameManager = XmlSession.getSession().getAttribute(Constantes.CURRENT_ALIAS_NAME_MANAGER);
		AliasNameManager globalAliasNameManager = XmlSession.getSession().getAttribute(Constantes.GLOBAL_ALIAS_NAME_MANAGER);
		String realClazz = null;
		if (realClazz == null && currentAliasNameManager != null) {
			realClazz = currentAliasNameManager.findRealName(alias);
		}
		if (realClazz == null && globalAliasNameManager != null) {
			realClazz = globalAliasNameManager.findRealName(alias);
		}
		if(realClazz == null){
			realClazz = alias;
		}
		Class<?> clazz = Class.forName(realClazz);
		return clazz;
	}
}
