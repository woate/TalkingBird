/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml;

import java.util.Map;

/**
 * XML会话对象
 * @author liucheng
 * 2014-12-19 上午08:22:00
 *
 */
public abstract class XmlSession{
	/**
	 * 通过工厂方法获取会话对象
	 * @return
	 */
	public static XmlSession getSession() {
		return XmlSessionFactory.getSession();
	}
	public static void destory() {
		XmlSessionFactory.destorySession();
	}
	/**
	 * 获取属性
	 * @param <T>
	 * @param name
	 * @return
	 */
	public abstract <T> T getAttribute(String name);
	/**
	 * 设置属性
	 * @param name
	 * @param val
	 */
	public abstract void setAttribute(String name, Object val);
	/**
	 * 获取所有属性
	 * @return
	 */
	public abstract Map<String, Object> attributes();
}
