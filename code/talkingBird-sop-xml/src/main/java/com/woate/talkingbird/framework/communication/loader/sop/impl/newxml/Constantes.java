/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml;

public interface Constantes {
	public static final String DOCUMENT = "DOCUMENT";
	public static final String XPATH = "XPATH";
	public static final String URI = "URI";
	public static final String PATH_STACK = "PATH_STACK";
	public static final String PARSING_NAME = "PARSING_NAME";
	/**
	 * 当前正在解析的对象
	 */
	public static final String CURRENT_OBJECT = "CURRENT_OBJECT";
	/**
	 * 当前正在解析的包节点类型
	 */
	public static final String CURRENT_PACKET_TYPE = "CURRENT_PACKET_TYPE";
	/**
	 * 当前正在解析的节点
	 */
	public static final String CURRENT_NODE = "CURRENT_NODE";
	public static final String CURRENT_HEADER = "CURRENT_HEADER";
	public static final String CURRENT_BUSI_LOADER = "CURRENT_BUSI_LOADER";
	public static final String CURRENT_MAIN_LOADER = "CURRENT_MAIN_LOADER";
	public static final String CURRENT_BOOTSTRAP_LOADER = "CURRENT_BOOTSTRAP_LOADER";
	public final static String METADATAS_POOL = "METADATAS_POOL";
	//-------------------别名映射节点-------------------------------------
	public final static String INTERFACES_ALIAS_MAPPING = "interfaces/alias-mappings";
	public final static String MAPPING = "mapping";
	public final static String ALIAS = "alias";
	public final static String REAL = "real";
	public final static String DOMAIN = "domain";
	public final static String INNER = "inner";
	public final static String GLOBAL = "global";
	/**
	 * 当前执行段的别名管理器
	 */
	public final static String CURRENT_ALIAS_NAME_MANAGER = "CURRENT_ALIAS_NAME_MANAGER";
	/**
	 * 全局别名管理器
	 */
	public final static String GLOBAL_ALIAS_NAME_MANAGER = "GLOBAL_ALIAS_NAME_MANAGER";
	//-------------------元信息池节点-------------------------------------
	public final static String INTERFACES_METADATS_POOL = "interfaces/metadatas-pool";
	public final static String FIELD = "field";
	public final static String FIELD_REF = "ref";
	public final static String FIELD_ALIAS = "alias";
	public final static String FIXED_FIELD = "fixed-field";
	public final static String FIXED_FIELD_NAME = "name";
	public final static String FIXED_FIELD_ALIAS = "alias";
	public final static String FIXED_FIELD_ENCODING = "encoding";
	public final static String FIXED_FIELD_DESC = "desc";
	public final static String FIXED_FIELD_FILL_STYLE = "fill-style";
	public final static String FIXED_FIELD_LEN = "len";
	public final static String FIXED_FIELD_CLASS = "class";
	public final static String FIXED_FIELD_VALUE_MODE = "value-mode";
	public final static String FIXED_FIELD_VALUE = "value";
	//------------------------------------------------------------------
	public final static String VARIABLE_FIELD = "variable-field";
	public final static String VARIABLE_FIELD_NAME = "name";
	public final static String VARIABLE_FIELD_ALIAS = "alias";
	public final static String VARIABLE_FIELD_ENCODING = "encoding";
	public final static String VARIABLE_FIELD_DESC = "desc";
	public final static String VARIABLE_FIELD_MAX_LEN = "max-len";
	public final static String VARIABLE_FIELD_MIN_LEN = "min-len";
	public final static String VARIABLE_FIELD_ALLOW_NULL = "allow-null";
	public final static String VARIABLE_FIELD_CLASS = "class";
	public final static String VARIABLE_FIELD_VALUE_MODE = "value-mode";
	public final static String VARIABLE_FIELD_VALUE = "value";
	public final static String INTERFACE_NAME = "name";
	public final static String INTERFACES = "interfaces";
	public final static String INTERFACES_INTERFACE = "interfaces/interface";
	
	public final static String OBJECT = "object";
	public final static String OBJECT_ALIAS = "alias";
	public final static String OBJECT_NAME = "name";
	public final static String OBJECT_TYPE = "type";
	public final static String OBJECT_DESC = "desc";
	public final static String OBJECT_ENCODING = "encoding";
	public final static String OBJECT_WRAP_CLASS = "wrap-class";
	
	public final static String BEAN = "bean";
	public final static String FORM = "form";
	public final static String TRADE = "trade";
	public final static String SYS = "sys";
	public final static String BUSI = "busi";
	public final static String BUSIS = "busis";
	public final static String INLINE = "inline";
	public final static String HEADER = "header";
	public final static String REQUEST = "request";
	public final static String RESPONSE = "response";
	public final static String EXCEPTION = "exception";
	
}
