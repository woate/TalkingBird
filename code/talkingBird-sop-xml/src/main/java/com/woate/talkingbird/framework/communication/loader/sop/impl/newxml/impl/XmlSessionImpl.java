/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.impl;

import java.util.HashMap;
import java.util.Map;

import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
/**
 * Xml解析过程中使用的会话实现
 * @author liucheng
 *
 */
public class XmlSessionImpl extends XmlSession{
	final Map<String, Object> attributes = new HashMap<String, Object>();
	@Override
	public void setAttribute(String name, Object val) {
		this.attributes.put(name, val);
	}

	@Override
	public Map<String, Object> attributes() {
		return this.attributes;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getAttribute(String name) {
		return (T) attributes.get(name);
	}
}
