/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml;

import java.net.URI;

import javax.xml.xpath.XPath;

import org.w3c.dom.Document;
/**
 * Xml文档解析器接口
 * @author liucheng
 *
 */
public interface XmlDocumentParser {
	
	/**
	 * 验证是否能够通过schema验证
	 */
	void vildateSchema(URI uri,String xsdPath) throws XmlParseException;
	/**
	 * 初始化获取dom模型根节点
	 * @param uri 解析文件路径
	 * @return Xml文档对象
	 * @throws XmlParseException Xml解析异常
	 */
	Document initDoc(URI uri) throws XmlParseException;
	/**
	 * 获取XPath
	 * @param uri 解析文件路径
	 * @return Xml文档对象
	 * @throws XmlParseException
	 */
	XPath initXpath(URI uri) throws XmlParseException;
}
