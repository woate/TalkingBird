/*
 *Copyright 1999-2012 Alibaba Group.
 * 
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * http://code.taobao.org/p/talkingbird/src/
 *
 */
package com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.delegate;

import java.util.ArrayList;
import java.util.List;




import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.woate.talkingbird.framework.communication.loader.PacketType;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.Constantes;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParseException;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlParser;
import com.woate.talkingbird.framework.communication.loader.sop.impl.newxml.XmlSession;
import com.woate.talkingbird.framework.communication.sop.metadata.SOPMetadata;
/**
 * 解析交易数据
 * @author liucheng
 *
 */
public class XmlHeaderParserDelegate implements XmlParser{
	XmlParser variableFieldParser = new XmlVariableFieldParserDelegate();
	XmlParser fixedFieldParser = new XmlFixedFieldParserDelegate();
	XmlParser fieldParser = new XmlFieldParserDelegate();
	@Override
	public void parse() throws XmlParseException {
		try {
			Node tradeNode = XmlSession.getSession().getAttribute(Constantes.CURRENT_NODE);
			NodeList nodeList = tradeNode.getChildNodes();
			PacketType type = XmlSession.getSession().getAttribute(Constantes.CURRENT_PACKET_TYPE);
			//在这里获取
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				String nodeName = node.getNodeName();
				if(type == PacketType.REQ && Constantes.REQUEST.equals(nodeName)){
					List<SOPMetadata> headers = parse0(node);
					XmlSession.getSession().setAttribute(Constantes.CURRENT_HEADER, headers);
				}else if(type == PacketType.RSP && Constantes.RESPONSE.equals(nodeName)){
					List<SOPMetadata> headers = parse0(node);
					XmlSession.getSession().setAttribute(Constantes.CURRENT_HEADER, headers);
				}else if(type == PacketType.EXP && Constantes.EXCEPTION.equals(nodeName)){
					List<SOPMetadata> headers = parse0(node);
					XmlSession.getSession().setAttribute(Constantes.CURRENT_HEADER, headers);
				}
				
			}
			
		} catch (XmlParseException e) {
			throw e;
		} catch (Exception e) {
			throw new XmlParseException(e);
		}
	}
	
	
	List<SOPMetadata> parse0(Node node) throws Exception{
		List<SOPMetadata> list = new ArrayList<SOPMetadata>();
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node fieldNode = nodeList.item(i);
			XmlSession.getSession().setAttribute(Constantes.CURRENT_NODE, fieldNode);
			String fieldName = fieldNode.getNodeName();
			if(Constantes.FIXED_FIELD.equals(fieldName)){
				fixedFieldParser.parse();
				SOPMetadata field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				list.add(field);
			}else if(Constantes.VARIABLE_FIELD.equals(fieldName)){
				variableFieldParser.parse();
				SOPMetadata field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				list.add(field);
			}else if(Constantes.FIELD.equals(fieldName)){
				fieldParser.parse();
				SOPMetadata field = XmlSession.getSession().getAttribute(Constantes.CURRENT_OBJECT);
				list.add(field);
			}
		}
		return list;
	}
}
